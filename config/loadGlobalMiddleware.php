<?php
$pipeline->load(new App\middleware\CatchExceptionMiddleware($this->container));
$pipeline->load(new App\middleware\PoweredByMiddleware($this->container));
$pipeline->load(new App\middleware\CsrfMiddleware($this->container));
$pipeline->load(new App\middleware\XssMiddleware($this->container));
