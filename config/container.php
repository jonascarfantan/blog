<?php
use Core\Http\Response\Responder;
use Core\App;
use Core\Renderer\TwigRenderer;
use Core\routing\Router;
use DI\Container;
use Nyholm\Psr7\Factory\Psr17Factory;
use Nyholm\Psr7Server\ServerRequestCreator;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\Dotenv\Dotenv;
use Twig\Environment;

return [
    //___________/
    //__APP____/
    //_______/
    App::class     => DI\factory(function(): App {
        return new \Core\App();
    }),
    
    //_________________/
    //__Container____/
    //_____________/
    \DI\Container::class => DI\factory(function(): Container {
        return $container = new DI\Container();
    }),
    
    //___________/
    //__DOT____/
    //_______/
    // load env variables
    Dotenv::class        => DI\factory(function() {
        $dotenv = new Dotenv();
        
        return $dotenv->load(dirname(__DIR__).DIRECTORY_SEPARATOR.'.env');
    }),
    
    //_____________/
    //__PSR17____/
    //_________/
    Psr17Factory::class  => new Nyholm\Psr7\Factory\Psr17Factory(),
    
    ServerRequestCreator::class => DI\factory(function(Psr17Factory $psr17_factory): ServerRequestInterface {
        $creator = new ServerRequestCreator(
            $psr17_factory,
            $psr17_factory,
            $psr17_factory,
            $psr17_factory,
        );
        return $serverRequest = $creator->fromGlobals();
    }),
    
    //_____________/
    //_RESPONDER_/
    //_________/
    Responder::class => DI\factory(function(Container $c): Responder{
        return new Responder($c);
    }),

    //______________/
    //__ROUTER____/
    //__________/
    Router::class               => Di\factory(function(Container $container, ServerRequestInterface $request): Router {
        return new Router($request, $container);
    }),
    
    //___________/
    //__TWIG___/
    //_______/
    Environment::class          => \DI\factory(function(): Environment {
        $loader = new \Twig\Loader\FilesystemLoader([VIEWS_PATH, SCRIPT_PATH, IMAGE_PATH]);
        $twig   = new \Twig\Environment($loader, []);
        $twig->addGlobal('views_path', VIEWS_PATH);
        $twig->addGlobal('views_path', VIEWS_PATH);
        //todo addGlobal for all immutable répétitive thing
        return $twig;
    }),
    
    //_______________/
    //__Renderer___/
    //___________/
    TwigRenderer::class         => DI\factory(function(Container $c): TwigRenderer {
        return $renderer = new \Core\Renderer\TwigRenderer($c->get(Environment::class));
    }),
    
    //____________________/
    //__CsrfMiddleware__/
    //________________/
//    CsrfMiddleware::class => DI\factory(function(Container $c): CsrfMiddleware {
//        return new CsrfMiddleware($c,$_SESSION,)
//    })

];
