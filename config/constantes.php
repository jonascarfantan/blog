<?php

//CORE FRAMWORK PATH
define('DS', DIRECTORY_SEPARATOR);
define('CONFIG_PATH', '..'.DS.'config'.DS);
define('GLOBAL_MIDDLEWARE_PATH', '..'.DS.'app'.DS.'middleware'.DS);
define('APP_PATH', '..'.DS.'app'.DS);

//FRONT-END PATH
//VIEW
define('VIEWS_PATH', '..'.DS.'resources'.DS.'views'.DS);

define('IMAGE_PATH', '..'.DS.'public'.DS.'img'.DS);
define('DOCUMENT_PATH', '..'.DS.'public'.DS.'doc'.DS);
define('SCRIPT_PATH', '..'.DS.'public'.DS.'javascript'.DS);
//ASSETS
define('STYLE_PATH', '..'.DS.'public'.DS.'css'.DS);
