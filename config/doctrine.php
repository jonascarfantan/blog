<?php
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
$entitiesPath = [
    'app'.DIRECTORY_SEPARATOR.'comment'.DIRECTORY_SEPARATOR.'domain'.DIRECTORY_SEPARATOR.'entity',
    'app'.DIRECTORY_SEPARATOR.'post'.DIRECTORY_SEPARATOR.'domain'.DIRECTORY_SEPARATOR.'entity',
    'app'.DIRECTORY_SEPARATOR.'auth'.DIRECTORY_SEPARATOR.'domain'.DIRECTORY_SEPARATOR.'entity',
    'app'.DIRECTORY_SEPARATOR.'tag'.DIRECTORY_SEPARATOR.'domain'.DIRECTORY_SEPARATOR.'entity',
];

$isDevMode                 = true;
$proxyDir                  = null;
$cache                     = null;
$useSimpleAnnotationReader = false;

// Connexion à la base de données
$dbParams = [
    'driver'   => 'pdo_mysql',
    'host'     => 'p5_blog_db',
    'charset'  => 'utf8',
    'user'     => 'root',
    'password' => 'toor',
    'dbname'   => 'p5_blog',
];

$config = Setup::createAnnotationMetadataConfiguration(
    $entitiesPath,
    $isDevMode,
    $proxyDir,
    $cache,
    $useSimpleAnnotationReader
);

return EntityManager::create($dbParams, $config);
