<?php
use function Http\Response\send;
session_set_cookie_params(0,'/','p5.local',true,true);
session_start();

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require '../vendor/autoload.php';
require '../config/constantes.php';

$entityManager = require '../config/doctrine.php';
$app           = new \Core\App();
$response      = $app->run();

send($response);


