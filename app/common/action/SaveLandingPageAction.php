<?php

namespace App\common\action;

use Core\Http\Response\Responder;
use Core\ADR\ActionInterface;
use Core\ADR\BaseAction;
use Core\Http\Validator\Validator;
use Psr\Http\Message\ServerRequestInterface;

class SaveLandingPageAction extends BaseAction implements ActionInterface {
    
    public function __invoke(ServerRequestInterface $request) {
        $data      = $request->getParsedBody();
        $validator = new Validator($data, 'Article');
        $errors    = $validator->required(['name', 'email', 'message'])
            ->string(['name', 'email', 'message'])
            ->email(['email'])
            ->getErrors();
        if(!empty($errors)) {
            $this->session->newMessage('error',$errors);
            return $this->responder->redirect('/contact', 302);
        }
        
//        $subject = $data['name'].' nous contact depuis le blog';
//        $message = $data['message'] . ' De la part de '.$data['name']. ' dont l\'adresse email est : '. $data['email'];
//        $target = 'mr.carfantan@yahoo.fr';
//        $message_ready = wordwrap($message, 70, "\r\n");
//        $mail = Mail::makeWelcome($target, $message_ready, $subject);
    
        try {
//            $mail->send();
        } catch(\Exception $e) {
            return $this->responder->exception($e->getMessage(), $e->getCode());
        }
    
    
        return $this->responder->redirect('/', 302);
    }
}
