<?php

namespace App\common\action;
use App\helper\AppHelper;
use App\helper\AuthHelper;
use Core\ADR\ActionInterface;
use Core\ADR\BaseAction;
use Psr\Http\Message\ServerRequestInterface;

class HomeAction extends BaseAction implements ActionInterface {
    
    public function __invoke(ServerRequestInterface $request) {
        $this->session->set('csrf_token', AppHelper::generateCsrf());
        $me = AuthHelper::me() ?? null;
        $page = [
            'title' => 'Tech for Human - Acceuil',
            'context' => 'fo',
            'session' => $this->session->getSession(),
            'me' => $me,
            'avatar' => AuthHelper::avatar(),
            'cv' => $me->upload->cv_path ?? null,
            'csrf_token' => $this->session->get('csrf_token'),
            
        ];
        return $this->responder->respond('fo.home.index', ['page' => $page]);
    }
}
