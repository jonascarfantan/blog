<?php

namespace App\common\action;
use App\helper\AppHelper;
use App\helper\AuthHelper;
use Core\ADR\ActionInterface;
use Core\ADR\BaseAction;
use Psr\Http\Message\ServerRequestInterface;

class MaintenancePageAction extends BaseAction implements ActionInterface {
    
    public function __invoke(ServerRequestInterface $request) {
        $me = AuthHelper::me() ?? null;
        $page = [
            'title' => 'Page en maintenance',
            'context' => 'fo',
            'session' => $this->session->getSession(),
            'me' => $me,
            'avatar' => AuthHelper::avatar(),
            'cv' => $me->upload->cv_path ?? null,
        ];
        return $this->responder->respond('error.workInProgress', ['page' => $page]);
    }
}
