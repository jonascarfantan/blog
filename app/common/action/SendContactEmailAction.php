<?php

namespace App\common\action;

use App\Responder\Responder;
use Core\ADR\ActionInterface;
use Core\ADR\BaseAction;
use Core\Http\Validator\Validator;
use Core\messenger\Mail;
use Psr\Http\Message\ServerRequestInterface;

class SendContactEmailAction extends BaseAction implements ActionInterface {
    
    public function __invoke(ServerRequestInterface $request) {
        $data      = $request->getParsedBody();
        $validator = new Validator($data, 'Article');
        $errors    = $validator->required(['name', 'email', 'message'])
            ->string(['name', 'email', 'message'])
            ->email(['email'])
            ->getErrors();
        if(!empty($errors)) {
            $this->session->newMessage('error',$errors);
            return $this->responder->redirect('/', 302);
        }
    
        try {
            $message = $data['message'] . "\r\n \r\n Envoyé par ".$data['name']. ' son adresse email  : '. $data['email'];
            $message_ready = wordwrap($message, 70, "\r\n");
            $mail = Mail::makeContact($message_ready, $data['name']);
            $mail->send();
        }catch(\Exception $e){
            return $this->responder->exception($e->getMessage(), $e->getCode());
        }
    
    
        return $this->responder->redirect('/', 302);
    }
}
