<?php

namespace App\common\action;

use App\helper\AppHelper;
use App\helper\AuthHelper;
use Core\Http\Response\Responder;
use Core\ADR\ActionInterface;
use Core\ADR\BaseAction;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class ContactPageAction extends BaseAction implements ActionInterface {
    
    public function __invoke(ServerRequestInterface $request) :ResponseInterface
    {
        $this->session->set('csrf_token', AppHelper::generateCsrf());
        $me = AuthHelper::me() ?? null;
        $page = [
            'title' => 'Contacter l\'agence',
            'context' => 'fo',
            'errors' => $this->session->getMessages('error'),
            'me' => $me ?? null,
            'avatar' => AuthHelper::avatar(),
            'csrf_token' => $this->session->get('csrf_token'),
        ];
        return $this->responder->respond('fo.home.contact', ['page' => $page]);
    }
}
