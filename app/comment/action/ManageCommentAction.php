<?php

namespace App\comment\action;

use App\comment\domain\entity\Comment;
use App\comment\domain\manager\CommentManager;
use App\comment\presenter\CommentPresenter;
use App\helper\AuthHelper;
use App\post\domain\entity\Post;
use App\auth\domain\entity\User;
use Core\ADR\ActionInterface;
use Core\ADR\BaseAction;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class ManageCommentAction extends BaseAction implements ActionInterface {
    
    public function __invoke(ServerRequestInterface $request): ResponseInterface {
        $commentManager = new CommentManager(['comment' => Comment::class, 'post' => Post::class, 'user' => User::class]);
        $comments = $commentManager->getAllComments();
        $page = [
            'context' => 'bo',
            'title' => 'Moderation des commentaires',
            'session' => $this->session->getSession(),
            'errors' => $this->session->getMessages('error'),
            'me' => AuthHelper::me(),
            'comments' => !($comments === null) ? CommentPresenter::prepareCommentToModerate($comments) : null,
        ];
        $this->session->deleteMessage('error');
        
        return $this->responder->respond('bo.blog.manageComment', ['page' => $page]);
    }
    
}
