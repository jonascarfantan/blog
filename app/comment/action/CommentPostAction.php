<?php

namespace App\comment\action;

use App\auth\domain\entity\User;
use App\comment\domain\entity\Comment;
use App\comment\domain\manager\CommentManager;
use App\post\domain\entity\Post;
use Core\Http\Response\Responder;
use Core\ADR\ActionInterface;
use Core\ADR\BaseAction;
use Core\Http\Validator\Validator;
use Psr\Http\Message\ServerRequestInterface;

class CommentPostAction extends BaseAction implements ActionInterface {
  
    public function __invoke(ServerRequestInterface $request) {
        $comment_body      = $request->getParsedBody();
        $post_id = $request->getQueryParams()['param'];
        
        $validator = new Validator($comment_body, 'Article');
        $errors    = $validator->required(['content'])
            ->string(['content'])
            ->getErrors();
        if(!empty($errors)) {
            $this->session->newMessage('error', $errors);
            return $this->responder->redirect('/post/'.$post_id, 302);
        }
        
        $commentManager = new CommentManager(['comment' => Comment::class, 'post' => Post::class, 'user' => User::class]);
        try {
            $commentManager->sendComment($comment_body, $post_id);
        }catch(\Exception $e){
            return $this->responder->exception($e->getMessage(), $e->getCode());
        }
        return $this->responder->redirect('/post/'.$post_id, 302);
        
    }
}
