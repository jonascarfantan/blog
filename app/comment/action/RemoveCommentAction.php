<?php

namespace App\comment\action;

use App\auth\domain\entity\User;
use App\comment\domain\entity\Comment;
use App\comment\domain\manager\CommentManager;
use App\post\domain\entity\Post;
use Core\ADR\ActionInterface;
use Core\ADR\BaseAction;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class RemoveCommentAction extends BaseAction implements ActionInterface {
   
    public function __invoke(ServerRequestInterface $request): ResponseInterface {
        $commentManager = new CommentManager(['comment' => Comment::class, 'post' => Post::class, 'user' => User::class]);
        $commentManager->remove($request);
        
        return $this->responder->redirect('/admin/comments', 302);
    }
}
