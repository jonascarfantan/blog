<?php

namespace App\comment\action;

use Core\ADR\ActionInterface;
use Core\ADR\BaseAction;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class ResponseCommentAction extends BaseAction implements ActionInterface {
    /**
     * @param ServerRequestInterface $request
     *
     * @return ResponseInterface
     *
     * @TODO Make a Response action actualy this is just a copy of CommentPost
     */
    public function __invoke(ServerRequestInterface $request) {
        return $this->responder->respond('fo.blog.readPost', []);
    }
}
