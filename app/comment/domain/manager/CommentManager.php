<?php

namespace App\comment\domain\manager;

use App\auth\domain\entity\User;
use App\comment\domain\entity\Comment;
use App\helper\AuthHelper;
use App\post\domain\entity\Post;
use Core\ADR\BaseEntityManager;
use Core\database\Manager;

use DI\NotFoundException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Psr\Http\Message\ServerRequestInterface;

class CommentManager extends BaseEntityManager {
    
    public function sendComment(array $comment_body, int $post_id): void
    {
        $post = $this->repos['post']->find($post_id);
        $user = $this->repos['user']->find(AuthHelper::me()->getId());
        $comment = new Comment($comment_body, $post, $user);
        $post->addComment($comment);
        $user->addComment($comment);
        $this->em->persist($comment);
        $this->em->flush();
    }
    
    public function showPostComments(ServerRequestInterface $request)
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select('c')
            ->from('App\comment\domain\entity\Comment', 'c')
            ->where('c.post = ' . $request->getQueryParams()['param'])
            ->andWhere('(c.moderated = 1)')
            ->orderBy('c.created_at', 'DESC');
        $query = $qb->getQuery();
    
        return $query->getResult();
    }
    
    public function getAllComments()
    {
        $qb = $this->em->createQueryBuilder();
            $qb->select('c')
            ->from('App\comment\domain\entity\Comment', 'c');
            $query = $qb->getQuery();
    
        return $query->getResult();
    }
    
    /**
     * @param ServerRequestInterface $request
     *
     * @throws NotFoundException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function approve(ServerRequestInterface $request)
    {
        $comment = $this->repos['comment']->find($request->getQueryParams()['param']);
        if ($comment !== null) {
            $comment->setModerated(true);
            $this->em->flush();
        } else {
            throw new NotFoundException('Ce commentaire n\'éxiste plus.', 404);
        }
    }
    
    /**
     * @param ServerRequestInterface $request
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function disapprove(ServerRequestInterface $request)
    {
        $comment = $this->repos['comment']->find($request->getQueryParams()['param']);
        if ($comment === null) {
            throw new NotFoundException('Ce commentaire n\'éxiste plus.', 404);
        }
        $comment->setModerated(false);
        $this->em->flush();
    }
    
    /**
     * @param ServerRequestInterface $request
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(ServerRequestInterface $request)
    {
        $comment = $this->repos['comment']->find($request->getQueryParams()['param']);
        $this->em->remove($comment);
        $this->em->flush();
    }

}
