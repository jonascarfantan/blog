<?php

namespace App\comment\domain\entity;
use App\auth\domain\entity\User;
use App\helper\DateHelpers;
use App\post\domain\entity\Post;
use Doctrine\ORM\Mapping as ORM;
/**
 * Class Comment
 *
 * @package App\Domain\Entity
 * @ORM\Entity
 * @ORM\Table(name="comment")
 */
class Comment {
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     */
    public $id;
    /**
     * @ORM\Column(type="text")
     */
    public $content;
    
    /**
     * @ORM\ManyToOne(targetEntity="App\post\domain\entity\Post", inversedBy="comments", cascade={"persist"})
     * @ORM\JoinColumn(name="post_id", referencedColumnName="id", onDelete="CASCADE")
     */
    public $post;
    
    /**
     * @ORM\ManyToOne(targetEntity="App\auth\domain\entity\User", inversedBy="comments", cascade={"persist"})
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id", onDelete="CASCADE")
     */
    public $author;
    
    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    public $moderated;
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    public $created_at;
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    public $updated_at = null;
    
    public function __construct(array $comment, Post $post, User $author) {
        $this->id         = $comment['id'] ?? null;
        $this->post       = $post;
        $this->author     = $author;
        $this->content    = $comment['content'];
        $this->moderated  = null;
        $this->created_at = DateHelpers::today();
    }
    
    public function setModerated($approved){
        $this->moderated = $approved;
    }
    
    public function setAuthor($author){
        return $this->author = $author;
    }
    
    public function getAuthor(){
        return $this->author;
    }
    
    public function setPost($post){
        return $this->post = $post;
    }
    
    public function getId(){
        return $this->id;
    }
    
    
}
