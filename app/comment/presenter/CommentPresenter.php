<?php

namespace App\comment\presenter;
use App\helper\DateHelpers;

class CommentPresenter {
    /**
     * @param array $comments_input
     *
     * @return array
     */
    public static function prepareComment(array $comments_input): array {
        
        $comments_output = [];
        foreach($comments_input as $comment){
            $date = (array)$comment->created_at;
            $comments_output[] = [
                'content' => $comment->content,
                'author' => $comment->author->pseudo,
                'sent_on' => DateHelpers::shortDate($date['date']),
            ];
        }
        return $comments_output;
    }
    
    /**
     * @param array $comments_input
     *
     * @return array
     */
    public static function prepareCommentToModerate(array $comments_input): array
    {
        $refused_comments = [];
        $new_comments = [];
        $approved_comments = [];
        
        foreach ($comments_input as $comment){
            $date = (array)$comment->created_at;
            $prepared_comment = [
                'id' => $comment->id,
                'post_title' => $comment->post->title,
                'content' => $comment->content,
                'author' => $comment->author->pseudo,
                'sent_on' => DateHelpers::shortDate($date['date']),
            ];
            
            if ($comment->moderated === null) {
                $new_comments[] = $prepared_comment;
            }
            elseif ($comment->moderated == false) {
                $refused_comments[] = $prepared_comment;
            }
            else {
                $approved_comments[] = $prepared_comment;
            }
        }
    
        return ['new' => $new_comments, 'refused' => $refused_comments, 'approved' => $approved_comments];
    }
}
