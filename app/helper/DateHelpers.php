<?php

namespace App\helper;

class DateHelpers {
    
    public static function today() {
        return new \DateTime('now', new \DateTimeZone('Europe/Paris'));
    }
    
    public static function readableDate(?string $date = null): ?string {
        if ($date) {
            $months_fr = [
                "", "janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août",
                "septembre", "octobre", "novembre", "décembre",
            ];
            $date    = preg_replace('#\s[0-9:\.]*#', '', $date);
            // on extrait la date du jour
            [$year, $months, $day] = explode('-', $date);
    
            return $day.' '.$months_fr[(integer)$months].' '.$year;
        }
        return null;
        
    }
    
    public static function shortDate(?string $date = null): ?string {
        if ($date != null) {
            $date = preg_replace('#\s[0-9:\.]*#', '', $date);
            $date = is_string($date) ? $date : $date['date'];
            [$year, $months, $day] = explode('-', $date);
    
            return $day.'/'.$months.'/'.$year;
        }
    
        return null;
    }
    
}
