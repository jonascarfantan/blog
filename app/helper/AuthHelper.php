<?php

namespace App\helper;

use App\auth\domain\entity\User;
use App\auth\domain\UserManager;
use App\auth\exception\UnexistingUserException;
use Core\SessionManager;
use phpDocumentor\Reflection\Types\Boolean;

final class AuthHelper {
    
    public static function me(): ?User {
        $session = new SessionManager($_SESSION);
        $manager = new UserManager(['user' => User::class]);
        $pseudo = $session->get('pseudo');
        if(isset($pseudo) AND !($pseudo === null)){
            return $manager->findUserByPseudo([ 'pseudo' => $pseudo]);
        }
        
        return null;
    }
    
    public static function avatar()
    {
        $me = self::me();
        if(isset($me->upload->avatar_path) AND !($me->upload->avatar_path === null)){
            return $me->upload->avatar_path;
        }
        return 'img/avatar/avatar.png';
    }
    
    /**
     * @param $profile
     *
     * @return bool
     * @throws UnexistingUserException
     */
    public static function amI(string $profile): Boolean {
        $session = new SessionManager($_SESSION);
        $manager = new UserManager(['user' => User::class]);
        $user = $manager->findUserByPseudo([ 'pseudo' => $session->getSessionKey('pseudo') ]);
        if($user['profile'] == $profile){
            return true;
        }else{
            return false;
        }
    }
    
    public static function connectedSession(User $user): SessionManager
    {
        $session = new SessionManager($_SESSION);
        $user_passport = [
            'pseudo' => $user->getPseudo(),
            'password' => $user->getPassword(),
            'role' => $user->getProfile(),
            'connected' => true,
            ];
        
        $session->connect($user_passport);
        
        return $session;
    }
    
}
