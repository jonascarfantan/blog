<?php

namespace App\helper;

use Exception;

class AppHelper {
    
    /**
     * @return string
     * @throws Exception
     */
    public static function generateCsrf(): string {
        return bin2hex(random_bytes(16));
    }
}
