<?php

namespace App\middleware;

use ArrayAccess;
use Core\exception\InvalidCsrfException;
use Core\exception\NoCsrfException;
use Core\SessionManager;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use TypeError;

class CsrfMiddleware implements MiddlewareInterface {
 
    private ContainerInterface $container;
    private ?string $sessionToken;
    private string $inputName;
    
    public function __construct(ContainerInterface $container){
        $session = new SessionManager($_SESSION);
        $this->container = $container;
        $this->sessionToken = $session->get('csrf_token');
        $this->inputName = 'csrf_token';
    }
    
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface {
        
        if(in_array($request->getMethod(), ['PUT', 'POST', 'DELETE', 'PATCH'])) {
            $params = $request->getParsedBody() ?? [];
            $params = isset($params['ajax_']) ? (array)json_decode($params['ajax_']) : $params;
            if(!array_key_exists($this->inputName, $params)) {
                
                //todo create 419 view for exception below
                throw new NoCsrfException('Il semblerait que vous essayez de valider un formulaire depuis un autre site, c\'est tout à fait proscrit.',403);
            }
            if ($params[$this->inputName] !== $this->sessionToken) {
                
                //todo same thing as above
                throw new InvalidCsrfException('Il semblerait que votre token csrf soit corrompu', 403);
            }
            
        }
            return $handler->handle($request);
    }
}
