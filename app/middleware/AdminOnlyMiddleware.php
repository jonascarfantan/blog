<?php

namespace App\middleware;

use Core\Http\Response\Responder;
use Core\SessionManager;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class AdminOnlyMiddleware implements MiddlewareInterface {
    private ContainerInterface $container;
    
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
        $this->responder = $this->container->get(Responder::class);
    }
    
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $session = new SessionManager($_SESSION);
        $role = $session->get('role');
        
        if($role === 'admin') {
            return $handler->handle($request);
        }
        return $this->responder->redirect('/posts', 403);
    
    }
    
}
