<?php

namespace App\middleware;

use Core\Http\Response\Responder;
use Nyholm\Psr7\Factory\Psr17Factory;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class PimpBodyResponseMiddleware implements MiddlewareInterface {
    
    /**
     * LoggedInMiddleware constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container){
        $this->container = $container;
        $this->responder = $this->container->get(Responder::class);
    }
    
    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     *
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface {
        $response    = $handler->handle($request);
        $httpFactory = new Psr17Factory();
        $stream      = $httpFactory->createStream('new content');
        
        return $response;
    }
}
