<?php

namespace App\middleware;

use Core\Http\Response\Responder;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class CatchExceptionMiddleware implements MiddlewareInterface {
    
    public ContainerInterface $container;
    public Responder $responder;
    
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->responder = $this->container->get(Responder::class);
    }
    
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        try {
            $response    = $handler->handle($request);
        } catch(\Exception $e) {
            $response = $this->responder->exception($e->getMessage(), $e->getCode());
        }
        
        return $response;
    }
}
