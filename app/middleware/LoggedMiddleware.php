<?php

namespace App\middleware;

use Core\Http\Response\Responder;
use Core\SessionManager;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;


class LoggedMiddleware implements MiddlewareInterface {
    
    private $responder;
    
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->responder = $this->container->get(Responder::class);
    }
    
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $session = new SessionManager($_SESSION);
        $connected = $session->get('connected');
        
        if(isset($connected) and $connected == true) {
            return $handler->handle($request);
        } else {
            return $this->responder->redirect('/connexion', 403);
        }
    
    }
}
