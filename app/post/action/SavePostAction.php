<?php

namespace App\post\action;

use App\auth\domain\entity\User;
use App\post\domain\entity\Post;
use App\tag\domain\entity\Tag;
use Core\ADR\ActionInterface;
use Core\ADR\BaseAction;
use Core\Http\Validator\Validator;
use Psr\Http\Message\ServerRequestInterface;

use App\post\domain\manager\PostManager;

class SavePostAction extends BaseAction implements ActionInterface {

    public function __invoke(ServerRequestInterface $request) {
        $postManager = new PostManager(['post' => Post::class, 'tag' => Tag::class, 'user' => User::class]);
        $validator = new Validator($request->getParsedBody(), 'Article');
        
        $errors    = $validator->required(['title', 'chapo', 'content'])
            ->string(['title', 'chapo', 'content'])
            ->getErrors();
        if(!empty($errors)) {
            $this->session->newMessage('error', $errors);
            
            return $this->responder->redirect('/admin/post/write', 302);
        }
        try {
            $postManager->createPost($request);
        }catch(\Exception $e){
            return $this->responder->exception($e->getMessage(), $e->getCode());
        }
        
        return $this->responder->redirect('/admin/posts', 302);
        
    }
}
