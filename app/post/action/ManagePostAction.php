<?php

namespace App\post\action;

use App\helper\AuthHelper;
use App\post\domain\entity\Post;
use App\post\domain\manager\PostManager;
use App\post\presenter\PostPresenter;
use Core\ADR\ActionInterface;
use Core\ADR\BaseAction;
use Psr\Http\Message\ServerRequestInterface;

class ManagePostAction extends BaseAction implements ActionInterface {
    
    public function __invoke(ServerRequestInterface $request) {
        $postManager = new PostManager(['post' => Post::class]);
        $page = [
            'context' => 'bo',
            'title' => 'Gestion des publications',
            'session' => $this->session->getSession(),
            'posts'   => PostPresenter::preparePosts($postManager->listPosts()),
            'avatar' => AuthHelper::avatar(),
            'me' => AuthHelper::me() ?? [],
        ];
        
        return $this->responder->respond('bo.blog.managePost', ['page' => $page]);
    }
}
