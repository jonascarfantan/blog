<?php

namespace App\post\action;


use App\helper\AuthHelper;
use App\post\domain\entity\Post;
use App\post\domain\manager\PostManager;
use App\post\presenter\PostPresenter;
use App\tag\domain\entity\Tag;
use App\tag\domain\TagManager;
use App\tag\presenter\TagPresenter;
use Core\ADR\ActionInterface;
use Core\ADR\BaseAction;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class ListPostAction extends BaseAction implements ActionInterface {
    
    public function __invoke(ServerRequestInterface $request): ResponseInterface
    {
        $tag_manager = new TagManager(['tag' => Tag::class]);
        $post_manager = new PostManager(['post' => Post::class]);
        
        $tags = $tag_manager->list();
        $posts = $post_manager->listPublished();
        $page = [
            'context' => 'fo',
            'title' => 'Publications',
            'session' => $this->session->getSession(),
            'posts'   => PostPresenter::preparePosts($posts),
            'tags'   => TagPresenter::prepareTags($tags),
            'avatar' => AuthHelper::avatar(),
            'me' => AuthHelper::me() ?? [],
        ];
        
        return $this->responder->respond('fo.blog.listPost', ['page' => $page]);
    }
    
}
