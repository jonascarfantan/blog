<?php

namespace App\post\action;

use App\helper\AppHelper;
use App\helper\AuthHelper;
use Core\ADR\ActionInterface;
use Core\ADR\BaseAction;
use Psr\Http\Message\ServerRequestInterface;

class WritePostAction extends BaseAction implements ActionInterface {
    
    public function __invoke(ServerRequestInterface $request) {
        $this->session->set('csrf_token', AppHelper::generateCsrf());
        $page        = [
            'context' => 'bo',
            'title' => 'Publication d\'article',
            'session' => $this->session->getSession(),
            'errors' => $this->session->getMessages('error'),
            'avatar' => AuthHelper::avatar(),
            'me' => AuthHelper::me() ?? [],
            'csrf_token' => $this->session->get('csrf_token'),
        ];
        $this->session->deleteMessage('error');
        return $this->responder->respond('bo.blog.editPost', ['page' => $page]);
    }
    
}
