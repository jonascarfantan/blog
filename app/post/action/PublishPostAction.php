<?php

namespace App\post\action;

use App\post\domain\entity\Post;
use Core\ADR\ActionInterface;
use Core\ADR\BaseAction;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

use App\post\domain\manager\PostManager;

class PublishPostAction extends BaseAction implements ActionInterface {
    
    public function __invoke(ServerRequestInterface $request): ResponseInterface {
        $postManager = new PostManager(['post' => Post::class]);
        $postManager->publish($request);
    
        return $this->responder->redirect('/admin/posts', 302);
    }
}
