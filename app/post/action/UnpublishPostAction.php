<?php

namespace App\post\action;

use App\post\domain\entity\Post;
use App\post\domain\manager\PostManager;
use Core\ADR\ActionInterface;
use Core\ADR\BaseAction;
use Psr\Http\Message\ServerRequestInterface;

class UnpublishPostAction extends BaseAction implements ActionInterface {
    
    public function __invoke(ServerRequestInterface $request) {
        $postManager = new PostManager(['post' => Post::class]);
        $postManager->unpublish($request);
        
        return $this->responder->redirect('/admin/posts', 302);
    }
}
