<?php

namespace App\post\action;

use App\helper\AppHelper;
use App\helper\AuthHelper;
use App\post\domain\entity\Post;
use App\post\domain\manager\PostManager;
use App\post\presenter\PostPresenter;
use Core\ADR\ActionInterface;
use Core\ADR\BaseAction;
use Psr\Http\Message\ServerRequestInterface;

class EditPostAction extends BaseAction implements ActionInterface {
    
    public function __invoke(ServerRequestInterface $request)
    {
        $me = AuthHelper::me();
        $this->session->set('csrf_token', AppHelper::generateCsrf());
        $postManager = new PostManager(['post' => Post::class]);
        try {
            $post = $postManager->getPost($request);
        } catch(\Exception $e) {
            return $this->responder->exception($e->getMessage(), $e->getCode());
        }
        
        $page = [
            'title' => 'Publier un article',
            'context' => 'bo',
            'session' => $this->session->getSession(),
            'errors' => $this->session->getMessages('error'),
            'csrf_token' => $this->session->get('csrf_token'),
            'me' => $me,
            'post' => PostPresenter::preparePost($post),
        ];

        $this->session->deleteMessage('error');
        return $this->responder->respond('bo.blog.editPost', ['page' => $page]);
    }
    
}
