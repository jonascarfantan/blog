<?php

namespace App\post\action;

use App\comment\domain\entity\Comment;
use App\comment\domain\manager\CommentManager;
use App\comment\presenter\CommentPresenter;
use App\helper\AuthHelper;
use App\post\domain\entity\Post;
use App\auth\domain\entity\User;
use App\post\domain\manager\PostManager;
use App\post\presenter\PostPresenter;
use App\tag\presenter\TagPresenter;
use Core\ADR\ActionInterface;
use Core\ADR\BaseAction;
use DI\NotFoundException;
use PDOException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class ReadPostAction extends BaseAction implements ActionInterface {
    
    public function __invoke(ServerRequestInterface $request): ResponseInterface {
        $me = AuthHelper::me() ?? null;
        $postManager = new PostManager(['post' => Post::class]);
        $commentManager = new CommentManager(['comment' => Comment::class, 'post' => Post::class, 'user' => User::class]);
    
        try {
            $post = $postManager->getPost($request);
            $comments = $commentManager->showPostComments($request);
        } catch(PDOException $e) {
            return $this->responder->exception($e->getMessage(), $e->getCode());
        } catch(NotFoundException $e) {
            return $this->responder->exception($e->getMessage(), $e->getCode());
        }
        
        $page = [
            'context' => 'fo',
            'title' => 'Article - '.$post->title,
            'session' => $this->session->getSession(),
            'csrf_token' => $this->session->get('csrf_token'),
            'errors' => $this->session->getMessages('error'),
            'post'   => PostPresenter::preparePost($post),
            'comments' => !($comments === null) ? CommentPresenter::prepareComment($comments) : null,
            'me' => $me ?? null,
            'avatar' => AuthHelper::avatar(),
        ];
        $this->session->deleteMessage('error');
        
        return $this->responder->respond('fo.blog.readPost', ['page' => $page]);
    }
    
}
