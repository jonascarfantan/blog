<?php

namespace App\post\action;

use App\post\domain\entity\Post;
use App\post\domain\manager\PostManager;
use App\tag\domain\entity\Tag;
use Core\ADR\ActionInterface;
use Core\ADR\BaseAction;
use Core\Http\Validator\Validator;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class UpdatePostAction extends BaseAction implements ActionInterface {
    
    public function __invoke(ServerRequestInterface $request): responseInterface {
        $validator = new Validator($request->getParsedBody(), 'Article');
        $postManager = new PostManager(['post' => Post::class, 'tag' => Tag::class]);
        
        $errors    = $validator->required(['title', 'chapo', 'content'])
            ->string(['title', 'chapo', 'content'])
            ->getErrors();
        if(!empty($errors)) {
            $this->session->newMessage('error', $errors);
            return $this->responder->redirect('/admin/post/'.$request->getQueryParams()['param'].'/edit', 302);
        }
        try {
            $postManager->updatePost($request);
        } catch(\Exception $e) {
            return $this->responder->exception($e->getMessage(), $e->getCode());
        }
        return $this->responder->redirect('/admin/posts', 200);
    }
}
