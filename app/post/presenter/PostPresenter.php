<?php

namespace App\post\presenter;
use App\helper\DateHelpers;

class PostPresenter {
    
    public static function preparePosts(array $all_posts){
        $posts_output = [];
        foreach($all_posts as $post){
            $post_tags = [];
            $wrote_date = (array)$post->created_at;
            $published_date = (array)$post->publication_date;
            
            foreach( $post->getTags() as $tag) {
                $post_tags[] = [
                    'id' => $tag->getId(),
                    'title' => $tag->getTitle(),
                    'color' => $tag->getColor(),
                ];
            }
            
            $posts_output[] = [
                'id' => $post->id,
                'title' => substr($post->title, 0, 64),
                'chapo' => substr($post->chapo, 0, 128),
                'content' => $post->content,
                'published' => $post->published,
                'author' => $post->author->pseudo,
                'tags' => $post_tags,
                'publication_date' => DateHelpers::readableDate($published_date['date']),
                'created_at' => DateHelpers::readableDate($wrote_date['date']),
            ];
        }
        
        return $posts_output;
    }
    
    /**
     * @param $post
     *
     * @return array
     */
    public static function preparePost($post): array {
        $posts_tags = [];
        foreach( $post->getTags() as $tag) {
            $posts_tags[] = [
                'id' => $tag->getId(),
                'title' => $tag->getTitle(),
                'color' => $tag->getColor(),
                ];
        }
        $date = (array)$post->created_at;
        if ($update_date = (array)$post->updated_at){
            $updated_date = $update_date['date'];
        } else {
            $updated_date = null;
        }
        
        return [
            'id' => $post->id,
            'title' => $post->title,
            'chapo' => $post->chapo,
            'content' => $post->content,
            'author' => $post->author->pseudo,
            'tags' => $posts_tags,
            'author_avatar' => $post->author->upload->avatar_path ?? 'img/avatar/avatar.png',
            'published' => $post->published,
            'publication_date' => DateHelpers::readableDate($date['date']),
            'updated_date' => DateHelpers::shortDate($updated_date),
        ];
    }
    
}
