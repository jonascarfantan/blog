<?php

namespace App\post\domain\manager;

use App\helper\AuthHelper;
use App\helper\DateHelpers;
use App\post\domain\entity\Post;
use Core\ADR\BaseEntityManager;
use DateTime;
use DI\NotFoundException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Psr\Http\Message\ServerRequestInterface;
use Exception;

class PostManager extends BaseEntityManager
{
    
    public function createPost(ServerRequestInterface $request): void
    {
        $my_id = AuthHelper::me()->getId();
        $form_data = $request->getParsedBody();
        $post_info = [];
        $tags = [];
        
        foreach($form_data as $key => $value) {
            if(is_integer($key)) {
                $tags[] = $this->repos['tag']->findOneBy(['id' => $value]);
                unset($form_data[$key]);
            } else {
                $post_info[$key] = $value;
            }
        }
        $user = $this->repos['user']->find($my_id);
        $post = new Post($post_info, $user);
        $user->addPost($post);
        foreach($tags as $tag) {
            $post->addTag($tag);
        }
        $this->em->persist($post);
        $this->em->flush();
    }
    
    
    
    public function updatePost(ServerRequestInterface $request)
    {
        $post_id = $request->getQueryParams()['param'];
        $form_data      = $request->getParsedBody();
        $post = $this->repos['post']->findOneBy(['id' => $post_id]);
        $actual_post_tags = $post->getTags();
        $actual_tags = [];
        
        foreach($actual_post_tags as $actual_tag) {
            $actual_tags[] = $actual_tag->id;
            }
        
        if( !empty($post) or $post !== null ) {
            foreach($form_data as $key => $value) {
                if(is_integer($key)) {
                    if(!in_array($key, $actual_tags)) {
                        $tag = $this->repos['tag']->findOneBy(['id' => $value]);
                        $post->addTag($tag);
                        unset($form_data[$key]);
                    }
                    $to_remove = array_search($key, $actual_tags);
                    if($to_remove or $to_remove === 0){
                        unset($actual_tags[$to_remove]);
                    }
                }
            }
            
            foreach($actual_tags as $tag_id) {
                $tag = $this->repos['tag']->findOneBy(['id' => $tag_id]);
                $post->removeTag($tag);
            }
            
            $post->update($form_data);
            $post->setUpdated(DateHelpers::today());
            
            $this->em->persist($post);
            $this->em->flush();
        } else {
            throw new NotFoundException('Ressource introuvable', 404);
        }
    }
    
    public function unpublish(ServerRequestInterface $request): void
    {
        $post = $this->repos['post']->find($request->getQueryParams()['param']);
        $post->setPublished(false);
        $this->em->flush();
    }
    
    public function publish(ServerRequestInterface $request): void
    {
        $post = $this->repos['post']->find($request->getQueryParams()['param']);
        if ($post !== null) {
            if ($post->publication_date === null) {
                $post->setPubDate(DateHelpers::today());
            }
            $post->setPublished(true);
            $this->em->flush();
        } else {
            throw new NotFoundException('Cette article n\'existe plus.', 404);
        }
        
    }
    
    public function getPost(ServerRequestInterface $request): Post
    {
        $post = $this->repos['post']->findOneBy(['id' => $request->getQueryParams()['param'] ]);
        if($post === null) {
            throw new NotFoundException('l\'Article est introuvable.', 404);
        }
        
        return $post;
    }
    
    public function listPosts(): array
    {
        return $this->repos['post']->findBy([], ['created_at' => 'desc']);
    }
    
    public function listPublished(): array
    {
        return $this->repos['post']->findBy(['published' => true], ['created_at' => 'desc']);
    }
    
    public function removePost(ServerRequestInterface $request): void
    {
        $post = $this->repos['post']->findOneBy(['id' => $request->getQueryParams()['param'] ]);
        if ($post !== null) {
            $this->em->remove($post);
            $this->em->flush();
        } else {
            throw new NotFoundException('Cette article n\'existe plus.', 404);
        }
    }
    
}
