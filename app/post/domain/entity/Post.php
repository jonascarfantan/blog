<?php

namespace App\post\domain\entity;

use App\auth\domain\entity\User;
use App\comment\domain\entity\Comment;
use App\helper\DateHelpers;
use App\tag\domain\entity\Tag;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Post
 *
 * @package App\Domain\Entity
 * @ORM\Entity
 * @ORM\Table(name="post")
 */
class Post {
    
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     */
    public $id;
    /**
     * @ORM\Column(type="string")
     */
    public $title;
    /**
     * @ORM\Column(type="text")
     */
    public $chapo;
    /**
     * @ORM\Column(type="text")
     */
    public $content;
    /**
     * @ORM\ManyToOne(targetEntity="App\auth\domain\entity\User", inversedBy="posts", cascade={"persist"})
     * @ORM\JoinColumn(name="author", referencedColumnName="id")
     */
    public $author;
    /**
     * @ORM\Column(type="boolean")
     */
    public $published;
    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    public $publication_date;
    /**
     * @ORM\Column(type="datetime")
     */
    public $created_at;
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    public $updated_at = null;
    
    /**
     * @ORM\OneToMany(targetEntity="App\comment\domain\entity\Comment", mappedBy="post")
     */
    public Collection $comments;
    
    /**
     * @ORM\ManyToMany(targetEntity="App\tag\domain\entity\Tag", mappedBy="posts")
     */
    private Collection $tags;
    
    public function __construct(array $post, User $author) {
        $this->id         = $post['id'] ?? null;
        $this->title      = $post['title'];
        $this->chapo      = $post['chapo'];
        $this->content    = $post['content'];
        $this->published  = $post['published'] ?? 0;
        $this->created_at = DateHelpers::today();
        $this->publication_date = $post['published'] === null ? null : DateHelpers::today();
    
        $this->author     = $author;
        $this->comments   = new ArrayCollection();
        $this->tags   = new ArrayCollection();
    }
    
    public function update(array $post): Post
    {
        foreach($post as $key => $value){
            $setter = 'set'.ucfirst($key);
            if( !is_integer($key) and method_exists($this, $setter) ) {
                $this->{$setter}($value);
            }
        }
        
        return $this;
    }
    
    // RELATIONALS OPERATIONS
    public function addTag(Tag $tag): Post
    {
        if(!$this->tags->contains($tag)) {
            $this->tags->add($tag);
            $tag->addPost($this);
        }
        
        return $this;
    }
    
    public function removeTag(Tag $tag): Post
    {
        if($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
            $tag->removePost($this);
        }
        
        return $this;
    }
    
    public function addComment(Comment $comment): Post { $this->comments->add($comment); return $this; }
    public function addAuthor(User $author): Post { $this->author = $author; return $this; }
    
    // SETTERS
    public function setId(int $id): Post { $this->id = $id; return $this; }
    public function setTitle(string $title): Post { $this->title = $title; return $this; }
    public function setChapo(string $chapo): Post { $this->chapo = $chapo; return $this; }
    public function setContent(string $content): Post { $this->content = $content; return $this; }
    public function setAuthor(User $author): Post { $this->author = $author; return $this; }
    public function setPublished(bool $published): Post { $this->published = $published; return $this; }
    public function setPubDate(Datetime $published): Post { $this->publication_date = $this->publication_date ?? $published; return $this; }
    public function setCreated(Datetime $date): Post { $this->created_at = $date; return $this; }
    public function setUpdated(Datetime $date): Post { $this->updated_at = $date; return $this; }
    
    // GETTERS
    public function getId(): string { return $this->id; }
    public function getTitle(): string { return $this->title; }
    public function getChapo(): string { return $this->chapo; }
    public function getContent(): string { return $this->content; }
    public function getAuthor(): string { return $this->author; }
    public function getTags() { return $this->tags; }
    
}
