<?php

namespace App\post\domain\exception;

class notFoundException extends \Exception {
    
    /**
     * notFoundException constructor.
     *
     * @param string $string
     */
    public function __construct(string $string) {
        parent::__construct();
        $this->message = "la resource $string n'a pas été trouvé dans notre système";
        $this->code = 404;
    }}
