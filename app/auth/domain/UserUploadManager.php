<?php

namespace App\auth\domain;

use App\auth\domain\entity\User;
use App\auth\domain\entity\UserUpload;
use App\auth\exception\UnexistingUserException;
use App\helper\AuthHelper;
use Core\ADR\BaseEntityManager;
use Core\database\Manager;
use Core\IO\Upload;
use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\TransactionRequiredException;
use Exception;
use Psr\Http\Message\ServerRequestInterface;

class UserUploadManager extends BaseEntityManager {
    
    public function showUserUpload(int $id){
        
        $qb = $this->em->createQueryBuilder();
        $qb->select('d')
            ->from('App\auth\domain\entity\UserUpload', 'd')
            ->where('d.user_id = '.$id);
        
        $query = $qb->getQuery();
        
        $rez = $query->getResult();
        return $rez;
    }
    
    public function savePath(Upload $upload): void {
        $my_id = AuthHelper::me()->getId();
        $user = $this->repos['user']->find($my_id);
        $user_upload = $this->repos['up']->findOneBy(['user_id' => $user->id]);
        
        $base_path = str_replace('../public/','',$upload->get('destination'));
        $path = $base_path . $user->id .'_'.$upload->filename ?? null;
        
        if(!$user_upload instanceof UserUpload){
            $user_upload = new UserUpload($user);
            $user_upload->addUser($user);
            $user->addUploadedFile($user_upload);
        }
        $user_upload->addFilePath($upload->category, $path);
            $this->em->persist($user_upload);
            $this->em->flush();
    }
    
}
