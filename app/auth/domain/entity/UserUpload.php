<?php

namespace App\auth\domain\entity;

use App\auth\domain\UserManager;
use App\helper\DateHelpers;
use Core\IO\Upload;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;

/**
 * @TODO CLEAN UP ENTITIES CLASS
 * @ORM\Entity
 * @ORM\Table(name="user_upload")
 */
class UserUpload {
    
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     */
    public $id;
    
    /**
     * @OneToOne(targetEntity="App\auth\domain\entity\User", cascade={"persist"})
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    public $user_id;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    public $avatar_path;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    public $cv_path;
    
    public function __construct(User $me)
    {
        $this->user = $me;
        
        return $this;
    }
    
    public function addFilePath( string $category, string $path): UserUpload
    {
        if($category == 'avatar') {
            $this->avatar_path = $path;
    
        } else {
            $this->cv_path = $path;
        }
        
        return $this;
    }
    
    public function addUser(User $user): UserUpload{
        $this->user_id = $user;
        return $this;
    }
    
    
}
