<?php

namespace App\auth\domain\entity;

use App\auth\domain\UserManager;
use App\helper\DateHelpers;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;

/**
 * @TODO CLEAN UP ENTITIES CLASS
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User {
    
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     */
    public $id;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    public $firstname;
    
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    public $lastname;
    
    /**
     * @ORM\Column(type="string", nullable=false)
     */
    public $pseudo;
    
    /**
     * @ORM\Column(type="string")
     */
    public $email;
    
    /**
     *@ORM\Column(type="string" )
     */
    public $profile = 'utilisateur';
    
    /**
     * @ORM\Column(type="string")
     */
    public $password;
    
    /**
     * @ORM\Column(type="boolean")
     */
    public $active;
    
    /**
     * @ORM\Column(type="datetime")
     */
    public $created_at;
    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    public $updated_at;
    
    /**
     * @ORM\OneToMany(targetEntity="App\comment\domain\entity\Comment", mappedBy="author")
     */
    public $comments;
    
    /**
     * @ORM\OneToMany(targetEntity="App\post\domain\entity\Post", mappedBy="author")
     */
    public $posts;
    
    /**
     * @ORM\OneToOne(targetEntity="App\auth\domain\entity\UserUpload", mappedBy="user_id")
     */
    public $upload;
    
    /**
     * @param array $user
     * @throws \Exception
     */
    public function __construct(array $user) {
        $this->id         = $user['id'] ?? null;
        $this->pseudo      = $user['pseudo'];
        $this->firstname  = $user['firstname'] ?? null;
        $this->lastname   = $user['lastname'] ?? null;
        $this->profile   = $user['profil'] ?? 'utilisateur';
        $this->email      = $user['email'];
        $this->password   = $user['password'];
        $this->active     = 1;
        $this->created_at = DateHelpers::today();
        
        $this->comments = new ArrayCollection();
    }
    
    public function update(array $new_values){
        foreach($new_values as $key => $value){
            if(property_exists($this, $key)){
                $this->$key = $value;
            }
        }
        return $this;
    }
    
    // RELATIONALS OPERATIONS
    public function addComment($comment): User { $this->comments->add($comment); return $this; }
    public function addPost($post): User { $this->posts->add($post); return $this; }
    public function addUploadedFile(UserUpload $upload): User { $this->upload = $upload; return $this; }
    
    // SETTERS
    public function setId($id) { $this->id = $id; return $this; }
    public function setFirstname($firstname) { $this->firstname = $firstname; return $this; }
    public function setLastname($lastname) { $this->lastname = $lastname; return $this; }
    public function setPseudo($pseudo) { $this->pseudo = $pseudo; return $this; }
    public function setEmail($email) { $this->email = $email; return $this; }
    public function setPassword($password) { $this->password = $password; return $this; }
    public function setActive($active) { $this->active = $active; return $this; }
    public function setUpdated($date) { $this->updated_at = $date; return $this; }
    
    // GETTERS
    public function getId() { return $this->id; }
    public function getProfile() { return $this->profile; }
    public function getFirstname() { return $this->firstname; }
    public function getLastname() { return $this->lastname; }
    public function getPseudo() { return $this->pseudo; }
    public function getEmail() { return $this->email; }
    public function getPassword() { return $this->password; }
    public function getActive() { return $this->active; }
    public function getUpdated() { return $this->updated_at; }
    
}
