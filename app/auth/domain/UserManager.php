<?php

namespace App\auth\domain;

use App\auth\domain\entity\User;
use App\auth\exception\UnexistingUserException;
use App\helper\AuthHelper;
use Core\ADR\BaseEntityManager;
use Core\SessionManager;
use DateTime;
use DI\NotFoundException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use Psr\Http\Message\ServerRequestInterface;

class UserManager extends BaseEntityManager {
    
    private SessionManager $session;
    
    public function users(): iterable
    {
        return $this->repos['user']->findBy([], ['created_at' => 'desc']);
    }
    
    public function findUserById($user_info): ?User
    {
        if(($user_info === null) or empty($user_info)) {
            throw new UnexistingUserException('Etrange, cet identifiant n\'éxiste plus', 404 );
        }
    
        return $this->repos['user']->findOneBy(['id' => $user_info]);
    }
    
    public function findUserByPseudo($user_info): ?User
    {
        if($user_info === null or empty($user_info)) {
            throw new UnexistingUserException('Etrange, le pseudo n\'est utilisé par personne', );
        }
    
        return $this->repos['user']->findOneBy(['pseudo' => $user_info['pseudo']]);
    }
    
    public function registerUser(array $user_info): void
    {
        $user_info['password'] = password_hash($user_info['password'], PASSWORD_BCRYPT);
        $user = new User($user_info);
        $this->em->persist($user);
        $this->em->flush();
    }
    
    public function updateMe(ServerRequestInterface $request): void
    {
        $my_id = $request->getQueryParams()['param'];
        $form_data  = $request->getParsedBody();
        $me    = $this->repos['user']->findOneBy(['id' => $my_id]);
        $user_info = [];
        foreach($form_data as $key => $value) {
            $user_info[$key] = $value;
        }
        $me->update($user_info);
        $me->setUpdated(new DateTime(date('Y-m-d')));
        
        AuthHelper::connectedSession($me);
        $this->em->persist($me);
        $this->em->flush();
    }
    
    public function updatePassword(ServerRequestInterface $request): void
    {
        $my_id = $request->getQueryParams()['param'];
        $passwords      = $request->getParsedBody();
        $me = $this->repos['user']->findOneBy(['id' => $my_id]);
        if(empty($me) or $me === null) {
            throw new Exception('Utilisateur introuvable', 404);
        }
        if(password_verify($passwords['old_password'], $me->getPassword())) {
            $me->password = password_hash($passwords['password'], PASSWORD_BCRYPT);
            $me->setUpdated(new DateTime(date('Y-m-d')));
            AuthHelper::connectedSession($me);
            $this->em->persist($me);
            $this->em->flush();
        } else {
            $this->session->newMessage('error', ['old_password' => 'Erreur mot de passe érroné']);
            //TODO debug EXCEPTION (create error page and good gestion of Exception) !!!
            throw new Exception('mot de passe érroné', 302);
        }
    }
    
    public function authenticate(array $connexionInfo): bool
    {
        $session = new SessionManager($_SESSION);
        $user = $this->repos['user']->findOneBy(['pseudo' => $connexionInfo['pseudo']]);
        if(!empty($user)) {
            if(password_verify($connexionInfo['password'], $user->getPassword())){
                AuthHelper::connectedSession($user);
                
                return true;
            } else {
                //todo Think about notice gestion
                $session->newMessage('error', ['wrongPsw' => 'Le mot de passe est incorrect']);
            }
        } else {
            //todo Think about notice gestion
            $session->newMessage('error', ['wrongUser' => 'L\'utilisateur n\'existe pas ou n\'est plus']);
        }
        
        return false;
    }
    
    public function switchStatus(ServerRequestInterface $request): void
    {
        $user = $this->repos['user']->find($request->getQueryParams()['param']);
        if ($user !== null) {
            if($user->getActive() == 0){
                $user->setActive(1);
            } else {
                $user->setActive(0);
            }
            $this->em->persist($user);
            $this->em->flush();
        } else {
            throw new NotFoundException('L\'utilisateur n\'existe plus.', 404);
        }
    }
    
    public function remove(ServerRequestInterface $request): void
    {
        $user = $this->repos['user']->find($request->getQueryParams()['param']);
        $this->em->remove($user);
        $this->em->flush();
    }
}
