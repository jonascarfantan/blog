<?php

namespace App\auth\action;

use App\auth\presenter\UserPresenter;
use App\helper\AppHelper;
use App\helper\AuthHelper;
use Core\ADR\ActionInterface;
use Core\ADR\BaseAction;
use Core\SessionManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class EditAccountPageAction extends BaseAction implements ActionInterface {
    
    public function __invoke(ServerRequestInterface $request): ResponseInterface {
        $this->session->set('csrf_token', AppHelper::generateCsrf());
        $me = AuthHelper::me();
        $page = [
            'title' => 'Mon compte - '. $me->pseudo,
            'context' => 'fo',
            'errors' => $this->session->getMessages('error'),
            'me' => UserPresenter::prepareAccount($me),
            'avatar' => AuthHelper::avatar(),
            'csrf_token' => $this->session->get('csrf_token'),
        ];
        $this->session->deleteMessage('error');
        
        return $this->responder->respond('fo.account.editAccount', ['page' => $page]);
    }
}
