<?php

namespace App\auth\action;

use Core\ADR\ActionInterface;
use Core\ADR\BaseAction;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class LogoutAction extends BaseAction implements ActionInterface {
    
    public function __invoke(ServerRequestInterface $request): ResponseInterface
    {
        $this->session->disconnect();
        
        return $this->responder->redirect('/posts',302);
    }
    
}
