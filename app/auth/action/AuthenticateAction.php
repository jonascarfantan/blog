<?php

namespace App\auth\action;

use App\auth\domain\entity\User;
use App\auth\domain\UserManager;
use Core\ADR\ActionInterface;
use Core\ADR\BaseAction;
use Core\Http\Validator\Validator;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class AuthenticateAction extends BaseAction implements ActionInterface {
    
    public function __invoke(ServerRequestInterface $request): ResponseInterface
    {
        $connexionInfo = $request->getParsedBody();
        $validator = new Validator($connexionInfo, 'Utilisateur');
        $errors    = $validator->required(['pseudo', 'password'])
            ->string(['pseudo', 'password'])
            ->getErrors();
        if(!empty($errors)) {
            $this->session->newMessage('error', $errors);
            return $this->responder->redirect('/connexion', 302);
        }
        
        $userManager = new UserManager(['user' => User::class]);
        $connected = $userManager->authenticate($connexionInfo);
    
        if($connected !== true){
            return $this->responder->redirect('/connexion', 302);
        } else {
            $this->session->deleteMessage('error');
            return $this->responder->redirect('/', 302);
        }
    }
}
