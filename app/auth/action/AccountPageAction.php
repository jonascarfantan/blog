<?php

namespace App\auth\action;

use App\auth\presenter\UserPresenter;
use App\helper\AppHelper;
use App\helper\AuthHelper;
use Core\Http\Response\Responder;
use Core\ADR\ActionInterface;
use Core\ADR\BaseAction;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class AccountPageAction extends BaseAction implements ActionInterface {
    
    public function __invoke(ServerRequestInterface $request): ResponseInterface {
        $this->session->set('csrf_token', AppHelper::generateCsrf());
        $me = AuthHelper::me();
        $page = [
            'title' => 'Mon compte - '. $me->pseudo,
            'context' => 'fo',
            'errors' => $this->session->getMessages('error'),
            'session' => $this->session->getSession(),
            'avatar' => AuthHelper::avatar(),
            'me' => UserPresenter::prepareAccount($me),
            'csrf_token' => $this->session->get('csrf_token'),
        ];
        $this->session->deleteMessage('error');
        return $this->responder->respond('fo.account.editAccount', ['page' => $page]);
    }
}
