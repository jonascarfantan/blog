<?php

namespace App\auth\action;

use App\auth\domain\entity\User;
use App\auth\domain\UserManager;
use App\auth\presenter\UserPresenter;
use App\helper\AppHelper;
use App\helper\AuthHelper;
use Core\Http\Response\Responder;
use Core\ADR\ActionInterface;
use Core\ADR\BaseAction;
use Core\Http\Validator\Validator;
use Psr\Http\Message\ServerRequestInterface;

class ChangePasswordAction extends BaseAction implements ActionInterface {
    
    public function __invoke(ServerRequestInterface $request) {
        $data = $request->getParsedBody();
        $me = AuthHelper::me();
        
        $validator = new Validator($data, 'Utilisateur');
        $errors = $validator
                    ->required(['old_password','password','password_confirm'])
                    ->password(['password'])
                    ->confirmPassword(['password','password_confirm'])
                    ->getErrors();
        if(!empty($errors)) {
            $this->session->newMessage('error', $errors);
            return $this->responder->redirect('/account/'.$me->id.'/edit', 302);
        }
        $userManager = new UserManager(['user' => User::class]);
        try {
            $userManager->updatePassword($request);
        } catch(\Exception $e) {
            return $this->responder->exception($e->getMessage(), $e->getCode());
        }
        $this->session->deleteMessage('error');
        return $this->responder->redirect('/account/'.$me->id, 302);
    }
}
