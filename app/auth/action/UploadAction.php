<?php

namespace App\auth\action;

use App\auth\domain\entity\User;
use App\auth\domain\entity\UserUpload;
use App\auth\domain\UserUploadManager;
use App\helper\AuthHelper;
use Core\ADR\ActionInterface;
use Core\ADR\BaseAction;
use Core\IO\Upload;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class UploadAction extends BaseAction implements ActionInterface {
    public function __invoke(ServerRequestInterface $request): ResponseInterface {
        $me = AuthHelper::me();
        $user_upload_manager = new UseruploadManager(['user' => User::class, 'up' => UserUpload::class]);
        
        try {
            $upload = new Upload($me->id);
            $upload->save();
            $user_upload_manager->savePath($upload);
            
        } catch(\Exception $e) {
            return $this->responder->exception($e->getMessage(), $e->getCode());
        }
        
        $this->session->deleteMessage('error');
        return $this->responder->redirect('/account/edit', 202);
    }
}
