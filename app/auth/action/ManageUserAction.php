<?php

namespace App\auth\action;

use App\auth\domain\entity\User;
use App\auth\domain\UserManager;
use App\auth\presenter\UserPresenter;
use App\helper\AuthHelper;
use Core\Http\Response\Responder;
use Core\ADR\ActionInterface;
use Core\ADR\BaseAction;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class ManageUserAction extends BaseAction implements ActionInterface {
    
    public function __invoke(ServerRequestInterface $request): ResponseInterface {
        $user_manager = new UserManager(['user' => User::class]);
        try {
            $users = $user_manager->users();
            
        } catch(\Exception $e) {
            return $this->responder->exception($e->getMessage(), $e->getCode());
        }
    
        $page = [
            'context' => 'bo',
            'title' => 'Gérer vos utilisateurs',
            'errors' => $this->session->getMessages('error'),
            'session' => $this->session->getSession(),
            'me' => AuthHelper::me(),
            'avatar' => AuthHelper::avatar(),
            'users' => !($users === null) ? UserPresenter::prepareUsers($users) : null,
        ];
        $this->session->deleteMessage('error');
    
        return $this->responder->respond('bo.user.manageUser', ['page' => $page]);
        
    }
}
