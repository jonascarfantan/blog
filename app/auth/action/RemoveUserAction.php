<?php

namespace App\auth\action;

use App\auth\domain\entity\User;
use App\auth\domain\UserManager;
use Core\ADR\ActionInterface;
use Core\ADR\BaseAction;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class RemoveUserAction extends BaseAction implements ActionInterface {
    
    public function __invoke(ServerRequestInterface $request): ResponseInterface {
        
        $user_manager = new UserManager(['user' => User::class]);
        $user_manager->remove($request);
        
        return $this->responder->redirect('/admin/users', 302);
    }
}
