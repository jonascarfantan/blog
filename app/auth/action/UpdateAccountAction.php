<?php

namespace App\auth\action;

use App\auth\domain\entity\User;
use App\auth\domain\UserManager;
use App\helper\AuthHelper;
use Core\Http\Response\Responder;
use Core\ADR\ActionInterface;
use Core\ADR\BaseAction;
use Core\Http\Validator\Validator;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;


class UpdateAccountAction extends BaseAction implements ActionInterface {

    public function __invoke(ServerRequestInterface $request): ResponseInterface {
        $data = $request->getParsedBody();
        $me = AuthHelper::me();
        
        $validator = new Validator($data, 'Utilisateur');
        $errors = $validator
            ->string(['pseudo', 'email', 'firstname', 'lastname', 'password', 'password_confirm'])
            ->email(['email'])
            ->getErrors();
        if(!empty($errors)) {
            $this->session->newMessage('error', $errors);
            return $this->responder->redirect('/account/'.$me->id.'/edit', 302);
        }
        $userManager = new UserManager(['user' => User::class]);
        try {
            $userManager->updateMe($request);
        } catch(\Exception $e) {
            return $this->responder->exception($e->getMessage(), $e->getCode());
        }
    
        $this->session->deleteMessage('error');
        return $this->responder->redirect('/account/'.$me->id, 202);
    }
}
