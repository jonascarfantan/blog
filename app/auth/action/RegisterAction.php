<?php

namespace App\auth\action;

use App\auth\domain\entity\User;
use App\auth\domain\UserManager;
use Core\Http\Response\Responder;
use Core\ADR\ActionInterface;
use Core\ADR\BaseAction;
use Core\Http\Validator\Validator;
use Core\messenger\Mail;
use Psr\Http\Message\ServerRequestInterface;

class RegisterAction extends BaseAction implements ActionInterface {

    public function __invoke(ServerRequestInterface $request) {
        $data = $request->getParsedBody();
        $validator = new Validator($data, 'Utilisateur');
        $errors = $validator->required(['pseudo', 'email', 'password', 'password_confirm'])
                    ->string(['pseudo', 'email', 'password', 'password_confirm'])
                    ->email(['email'])
                    ->password(['password'])
                    ->confirmPassword(['password','password_confirm'])
                    ->getErrors();
        if(!empty($errors)) {
            $this->session->newMessage('error', $errors);
            return $this->responder->redirect('/connexion', 302);
        }
        $userManager = new UserManager(['user' => User::class]);
        try {
            $userManager->registerUser($data);
            $mail = Mail::makeWelcome($data['email'], $data['pseudo']);
            $mail->send();
            
        } catch(\Exception $e) {
            return $this->responder->exception($e->getMessage(), $e->getCode());
        }
        $this->session->deleteMessage('error');
        return $this->responder->redirect('/', 302);
    }
}
