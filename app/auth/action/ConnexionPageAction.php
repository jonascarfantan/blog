<?php

namespace App\auth\action;

use App\helper\AppHelper;
use App\helper\AuthHelper;
use Core\Http\Response\Responder;
use Core\ADR\ActionInterface;
use Core\ADR\BaseAction;
use Core\Response\TwigResponder;
use Core\SessionManager;
use DI\Container;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

final class ConnexionPageAction extends BaseAction implements ActionInterface {
    
    public function __invoke(ServerRequestInterface $request): ResponseInterface {
        $this->session->set('csrf_token', AppHelper::generateCsrf());
        $page = [
            'title' => 'Connexion - Inscription',
            'context' => 'fo',
            'errors' => $this->session->getMessages('error'),
            'avatar' => AuthHelper::avatar(),
            'csrf_token' => $this->session->get('csrf_token'),
        ];
        $this->session->deleteMessage('error');
        
        return $this->responder->respond('fo.home.connexion', ['page' => $page]);
    }
}
