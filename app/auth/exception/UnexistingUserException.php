<?php

namespace App\auth\exception;

use phpDocumentor\Reflection\Types\Integer;

class UnexistingUserException extends \Exception {
    
    public function __construc(string $message, int $code){
        parent::__construct($message, $code);
    }
}
