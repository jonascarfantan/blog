<?php

namespace App\auth\exception;

class ForbiddenException extends \Exception {
    public function __construc(string $message, int $code){
        parent::__construct($message, $code);
    }

}
