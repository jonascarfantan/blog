<?php

namespace App\auth\presenter;
use App\auth\domain\entity\User;
use App\helper\DateHelpers;

class UserPresenter {
    /**
     * @param array $users_input
     *
     * @return array
     */
    public static function prepareUsers(array $users_input): array {
        $users_output = [];
        foreach($users_input as $user){
            $created_at = (array)$user->created_at;
            $users_output[] = [
                'id' => $user->id,
                'firstname' => $user->firstname,
                'lastname' => $user->lastname,
                'pseudo' => $user->pseudo,
                'profile' => $user->profile,
                'email' => $user->email,
                'status' => $user->active,
                'created_at' => DateHelpers::shortDate($created_at['date']),
            ];
        }
        
        return $users_output;
    }
    
    /**
     * @param User $user
     *
     * @return array
     */
    public static function prepareAccount(User $user): array {
        $created_at = (array)$user->created_at;
        
            return [
                'id' => $user->id,
                'firstname' => $user->firstname,
                'lastname' => $user->lastname,
                'pseudo' => $user->pseudo,
                'profile' => $user->profile,
                'email' => $user->email,
                'status' => $user->active,
                'created_at' => DateHelpers::shortDate($created_at['date']),
            ];
    }
}

