<?php

namespace App\tag\action;

use App\tag\domain\entity\Tag;
use App\tag\domain\TagManager;
use Core\ADR\ActionInterface;
use Core\ADR\BaseAction;
use Psr\Http\Message\ServerRequestInterface;

class ListTagAction extends BaseAction implements ActionInterface {
    
    public function __invoke(ServerRequestInterface $request) {
        $tag_manager = new TagManager(['tag' => Tag::class]);
        $tags = $tag_manager->list();
        
        return $this->responder->json(['tags' => $tags]);
    }
}
