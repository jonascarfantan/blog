<?php

namespace App\tag\action;

use App\tag\domain\entity\Tag;
use App\tag\domain\TagManager;
use Core\ADR\ActionInterface;
use Core\ADR\BaseAction;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class NewTagAction extends BaseAction implements ActionInterface {
    
    public function __invoke(ServerRequestInterface $request) {
        $tag_info = $request->getParsedBody();
        $tag_manager = new TagManager(['tag' => Tag::class]);
        $tag_info = isset($tag_info['ajax_']) ? (array)json_decode($tag_info['ajax_']) : $tag_info;
        
        try {
            $tag_manager->create($tag_info);
            $data = ['message' => 'Le nouveau tag à été enregistré'];
        }catch(\Exception $e){
            return $this->responder->exception($e->getMessage(), $e->getCode());
        }
        
        return $this->responder->json($data);
    }
}
