<?php

namespace App\tag\action;

use App\tag\domain\entity\Tag;
use App\tag\domain\TagManager;
use Core\ADR\ActionInterface;
use Core\ADR\BaseAction;
use Psr\Http\Message\ServerRequestInterface;

final class RemoveTagAction extends BaseAction implements ActionInterface {
    
    public function __invoke(ServerRequestInterface $request) {
        
        $tag_manager = new TagManager(['tag' => Tag::class]);
        try {
            $tag_manager->remove($request);
            $message = ['message' => 'Tag supprimé.'];
        } catch(\Exception $e) {
            return $this->responder->exception($e->getMessage(), $e->getCode());
        }
        return $this->responder->json($message);
    }
}
