<?php

namespace App\tag\domain;

use App\tag\domain\entity\Tag;
use Core\ADR\BaseEntityManager;
use DI\NotFoundException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Psr\Http\Message\ServerRequestInterface;

class TagManager extends BaseEntityManager {
    
    public function list(): array
    {
    $tags = $this->repos['tag']->findAll();
    return $tags;
    }
    
    public function create(array $tag_info): void
    {
        $tag = new Tag($tag_info);
        $this->em->persist($tag);
        $this->em->flush();
    }
    
    public function remove(ServerRequestInterface $request): void{
        $tag = $this->repos['tag']->findOneBy(['id' => $request->getQueryParams()['param'] ]);
        if ($tag !== null) {
            $this->em->remove($tag);
            $this->em->flush();
        } else {
            throw new NotFoundException('Ce tag n\'existe pas.');
        }
    }
}
