<?php

namespace App\tag\domain\entity;

use App\post\domain\entity\Post;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToMany;

/**
 * Class Post
 *
 * @package App\Domain\Entity
 * @ORM\Entity
 * @ORM\Table(name="tag")
 */
class Tag {
    
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     */
    public $id;
    
    /**
     * @ORM\Column(type="string")
     */
    public $title;
    
    /**
     * @ORM\Column(type="text")
     */
    public $color;
    
    /**
     * @ManyToMany(targetEntity="App\post\domain\entity\Post")
     * @JoinTable(name="post_tag",
     *  joinColumns={@JoinColumn(name="tag_id", referencedColumnName="id")},
     *  inverseJoinColumns={@JoinColumn(name="post_id", referencedColumnName="id")}
     *  )
     */
    public Collection $posts;
    
    public function __construct(array $tag)
    {
        $this->title = $tag['title'];
        $this->color = $this->colorGen();
        
        $this->posts = new ArrayCollection();
    }
    
    // SETTER
    public function setTitle(string $title) { $this->title = $title; return $this ; }
    public function setColor(string $color) { $this->color = $color; return $this ; }
    // GETTER
    public function getId() { return $this->id; }
    public function getTitle() { return $this->title; }
    public function getColor() { return $this->color; }
    public function getPosts(): Collection { return $this->posts; }
    
    // RELATIONAL OPERATIONS
    public function addPost(Post $post): Tag
    {
        if(!$this->posts->contains($post)) {
            $this->posts->add($post);
            $post->addTag($this);
        }
    
        return $this;
    }
    
    public function removePost(Post $post): Tag
    {
        if($this->posts->contains($post)) {
            $this->posts->removeElement($post);
            $post->removeTag($this);
        }
        
        return $this;
    }
    
    private function colorGen(): string
    {
        $color = dechex(mt_rand(0,16777215));
        $color = str_pad($color,6,'0');
        
        return $color;
    }
}
