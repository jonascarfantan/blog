<?php

namespace App\tag\presenter;

class TagPresenter {
    
    public static function prepareTags(array $tags): array
    {
        $tags_output = [];
            foreach( $tags as $tag) {
                $tags_output[] = [
                    'id' => $tag->getId(),
                    'title' => $tag->getTitle(),
                    'color' => $tag->getColor(),
                ];
            }
            
        return $tags_output;
    }
    
}
