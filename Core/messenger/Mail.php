<?php

namespace Core\messenger;

use phpDocumentor\Reflection\Types\Boolean;
use Swift_Message;
use Swift_SmtpTransport;

class Mail {
    
    private string $target;
    private string $message;
    private string $subject;
    
    private function __construct(string $target, string $message, string $subject)
    {
        $this->target = $target;
        $this->message = $message;
        $this->subject = $subject;
    }
    
    /**
     * @param string $target
     * @param string $pseudo
     *
     * @return Mail
     */
    public static function makeWelcome(string $target, string $pseudo): Mail
    {
        $subject = "Bienvenue sur Gaia tech";
        $message = "Bienvenue $pseudo, Heureux de te voir sur le blog Gaia tech. \n\r
        Si tu pense que c'est la tech qui doit être au service de la Terre et des Hommes et non le contraire, \n\r
        Si tu es un artisan pationné par le software, \n\r
        alors tu vas te sentir ici, comme chez toi. \n\r
        Met toi à l'aise, lis, parle, partage.";
        $message_ready = wordwrap($message, 70, "\r\n");
        return new self($target, $message_ready, $subject);
    }
    
    /**
     * @param string $message
     * @param string $name
     *
     * @return Mail
     */
    public static function makeContact(string $message, string $name): Mail
    {
        $subject = "Hey, $name vous à écris depuis contact";
        $target = "mr.carfantan@yahoo.fr";
        
        return new self($target, $message, $subject);
    }
    
    /**
     * @return $this
     */
    public function send(): int
    {
        $transport = (new Swift_SmtpTransport('smtp.gmail.com', 587, 'tls'))
            ->setUsername('studio.flashbang@gmail.com')
            ->setPassword('djossouvilynda');
        $mailer = new \Swift_Mailer($transport);
        $message = (new Swift_Message('Wonderful Subject'))
            ->setFrom(['studio.flashbang@gmail.com' => 'Jonas carfantan'])
            ->setSubject($this->subject)
            ->setTo($this->target)
            ->setBody($this->message);
        try {
            $sended = $mailer->send($message);
//            mail($this->target, $this->subject, $this->message);
        } catch (\Exception $e) {
            die($e->getMessage());
        }
        return $sended;
    }
    
}
