<?php

namespace Core\Http\Validator;


use phpDocumentor\Reflection\Types\This;

class Validator {
    
    private array $params;
    private $errors = [];
    
    public function __construct(array $params, $context) {
        $this->params         = $params;
        $this->errorGenerator = new ValidationError($context);
    }
    
    /**
     * Ajoute une erreur
     *
     * @param string $key
     * @param string $rule
     */
    private function addError(string $key, string $rule): void {
        if(!isset($this->errors[$key])) {
            $this->errors[$key] = $this->errorGenerator->getErrorMessage($key, $rule);
        }
    }
    
    /**
     * Récupère les erreurs
     *
     * @return array
     */
    public function getErrors(): array {
        return $this->errors;
    }
    
    /**
     * @param string $key
     *
     * @return mixed|null
     */
    private function getValue(string $key) {
        if(array_key_exists($key, $this->params)) {
            return $this->params[$key];
        }
        
        return null;
    }
    
    /**
     * Vérifie que l'élément est bien renseigné
     *
     * @param string ...$keys
     *
     * @return $this
     */
    public function required(array $keys): Validator {
        foreach($keys as $key) {
            $value = $this->getValue($key);
            if($value === null or empty($value)) {
                $this->addError($key, 'required');
            }
        }
        
        return $this;
    }
    
    /**
     * Vérifie que l'élément est une chaine de caractère
     *
     * @param array $keys
     *
     * @return $this
     */
    public function string(array $keys): Validator {
        foreach($keys as $key) {
            $value = $this->getValue($key);
            if(!($value === null) && !is_string($this->params[$key])) {
                $this->addError($key, 'string');
            }
        }
        
        return $this;
    }
    
    /**
     * @param array $keys
     *
     * @return $this
     */
    public function integer(array $keys): Validator {
        foreach($keys as $key) {
            $value = $this->getValue($key);
            if(!($value === null) && !is_integer($this->params[$key])) {
                $this->addError($key, 'integer');
            }
        }
        
        return $this;
    }
    
    /**
     * @param array $keys
     *
     * @return $this
     */
    public function boolean(array $keys): Validator {
        foreach($keys as $key) {
            $value = $this->getValue($key);
            if(!($value === null) && !is_bool($this->params[$key])) {
                $this->addError($key, 'boolean');
            }
        }
        
        return $this;
    }
    
    /**
     * @param array $keys
     *
     * @TODO debugger ça
     * @return $this
     */
    public function length(array $keys): Validator {
        foreach($keys as $key => $value) {
            $param_value = $this->getValue($key);
            if(strlen($param_value) > $value[0] and strlen($param_value) < $value[1]) {
                $this->addError($key, 'length');
            }
        }
        
        return $this;
    }
    
    /**
     * @param array $keys
     *
     * @return $this
     */
    public function email(array $keys): Validator {
        foreach($keys as $key) {
            $value = $this->getValue($key);
            $valide = preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i", $value);
            if($valide == 0) {
                $this->addError($key, 'email');
            }
        }
        return $this;
    }
    
    /**
     * @param array $keys
     *
     * @return $this
     */
    public function password(array $keys): Validator{
            foreach($keys as $key) {
                $value = $this->getValue($key);
                if(isset($value) and !empty($value)){
                    $valide = preg_match("/^(?=.*[!@#$%^&*-])(?=.*[0-9])(?=.*[A-Z]).{8,20}$/", $value);
                    if(!$valide) {
                        $this->addError($key, 'password');
                    }
            }
        }
        return $this;
    }
    
    /**
     * @param array $keys
     *
     * @return $this
     */
    public function confirmPassword(array $keys): Validator{
        $psw = $this->getValue($keys[0]);
        $psw_confirm = $this->getValue($keys[1]);
        if(isset($psw_confirm) and !($psw_confirm === null)) {
            if($psw !== $psw_confirm) {
                $this->addError($keys[1], 'password_confirm');
            }
        }
        return $this;
    }
    
}
