<?php

namespace Core\Http\Validator;

class ValidationError {
    
    private string $key;
    private string $rule;
    private string $formSubject;
    public array $messages = [
        'required' => 'est requis',
        'string'   => 'doit contenir du texte',
        'integer'  => 'doit contenir un entier',
        'boolean'  => 'doit contenir vrai ou faux',
        'notEmpty' => 'ne peut être vide',
        'length'   => 'n\'a pas la bonne taille',
        'email'   => 'doit être une addresse email valide',
        'password'   => 'n\'est pas assez sécuritaire ',
        'password_confirm'   => 'doit être identique ',
    ];
    
    public function __construct($form_subject) {
        $this->formSubject = $form_subject;
    }
    
    public function getErrorMessage(string $key, string $rule) {
        $this->key = $key;
        switch($rule) {
            case 'required':
                return $this->formSubject.' a besoin d\'un '.$this->key;
                break;
            case 'integer':
                return $this->key.' de l\''.$this->formSubject.' doit être un entier ';
                break;
            case 'string':
                return $this->key.' de l\''.$this->formSubject.' doit être du texte ';
                break;
            case 'boolean':
                return $this->key.' de l\''.$this->formSubject.' doit être vrai ou faux ';
                break;
            case 'email':
                return $this->key.' de l\''.$this->formSubject.' doit être une adresse valide ';
                break;
            case 'password':
                return $this->key.' de l\''.$this->formSubject.' n\'est pas assez sécuritaire ';
                break;
            case 'password_confirm':
                return $this->key.' de l\''.$this->formSubject.' doit être identique ';
                break;
        }
        
    }
}
