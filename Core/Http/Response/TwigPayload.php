<?php

namespace Core\Response;

use Psr\Http\Message\ServerRequestInterface;

class TwigPayload implements PayloadInterface {
    
    private ServerRequestInterface $request;
    private $template;
    private $data;
    
    /**
     * TwigPayload constructor.
     *
     * @param ServerRequestInterface $request
     * @param string $template
     * @param array $data
     */
    public function __construct(ServerRequestInterface $request, string $template, array $data = []) {
        $this->request  = $request;
        $this->template = $template;
        $this->data     = $data;
    }
    
    /**
     * @return ServerRequestInterface
     */
    public function getRequest(): ServerRequestInterface {
        return $this->request;
    }
    
    /**
     * @return string
     */
    public function getTemplate(): string {
        return $this->template;
    }
    
    /**
     * @return array
     */
    public function getData(): array {
        return $this->data;
    }
}
