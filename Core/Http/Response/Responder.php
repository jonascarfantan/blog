<?php

namespace Core\Http\Response;

use Core\App;
use Core\Renderer\TwigRenderer;
use DI\Container;
use Nyholm\Psr7\Factory\Psr17Factory;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;

class Responder {
    
    protected TwigRenderer $renderer;
    protected Container $container;
    protected Psr17Factory $httpFactory;
    private App $app;
    
    public function __construct(ContainerInterface $container) {
        $this->renderer    = $container->get(TwigRenderer::class);
        $this->httpFactory = $container->get(Psr17Factory::class);
        $this->app         = $container->get(App::class);
    }
    
    public function exception($message, $code): ResponseInterface
    {
        $page = [
            'error_msg' => $message,
            'error_code' => $code,
        ];
        
        $render = $this->renderer->render('error.error', ['page' => $page]);
        $stream = $this->httpFactory->createStream($render);
        
        return $this->httpFactory
            ->createResponse($code)
            ->withBody($stream);
    }
    
    public function respond(string $view, array $data = []): ResponseInterface
    {
        $render = $this->renderer->render($view, $data);
        $stream = $this->httpFactory->createStream($render);
    
        return $this->httpFactory
            ->createResponse(200)
            ->withBody($stream);
        
    }
    
    public function json(array $data, $code = 200): ResponseInterface
    {
        $stream = $this->httpFactory->createStream(json_encode($data));
        
        return $this->httpFactory
            ->createResponse($code)
            ->withBody($stream);
    }
    
    public function redirect(string $uri, int $code): ResponseInterface
    {
        return $this->httpFactory
            ->createResponse($code)
            ->withHeader('Location', $uri);
    }
}
