<?php

namespace Core\Response;

use Core\ResponderInterface;
use Psr\Http\Message\ServerRequestInterface;

class TwigResponder {
    
    private $responder;
    
    public function __construct(ResponderInterface $responder) {
        $this->responder = $responder;
    }
    
    public function respond(ServerRequestInterface $request, PayloadInterface $payload) {
        $response = $this->responder->respond($payload);
        
        if(!$payload instanceof TwigResponder) {
        
        }
        
        return $response;
    }
}
