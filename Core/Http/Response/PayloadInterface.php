<?php

namespace Core\Response;

use Psr\Http\Message\ServerRequestInterface;

interface PayloadInterface {
    
    public function getRequest(): ServerRequestInterface;
    
}
