<?php

namespace Core;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Core\Response\PayloadInterface;

interface ResponderInterface {
    
    public function respond(ServerRequestInterface $request, PayloadInterface $payload): ResponseInterface;
    
}
