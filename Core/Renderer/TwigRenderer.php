<?php

namespace Core\Renderer;

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class TwigRenderer {
    
    public Environment $twig;
    
    public function __construct(Environment $twig) {
        $this->twig = $twig;
    }
    
    /**
     * @param string $view
     * @param array $data
     *
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function render(string $view, array $data = []): string {
        $view_path = str_replace('.', DIRECTORY_SEPARATOR, $view);
        
        $template  = $this->twig->load($view_path.'.html.twig');
        
        return $template->render($data);
    }
    
    //    public function loadResponse()
    
}
