<?php

namespace Core\ADR;

use Core\Http\Response\Responder;
use Core\SessionManager;
use Psr\Container\ContainerInterface;

abstract class BaseAction {
    
    protected Responder $responder;
    protected ContainerInterface $container;
    protected SessionManager $session;
    
    public function setContainer(ContainerInterface $container) {
        $this->container = $container;
        
        return $this;
    }
    
    public function setResponder(ContainerInterface $container) {
        $this->responder = new Responder($container);
        
        return $this;
    }
    
    public function setSession(SessionManager $sessionManager) {
        $this->session = $sessionManager;
        
        return $this;
    }
}
