<?php

namespace Core\ADR;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

interface ActionInterface {
    
    public function __invoke(ServerRequestInterface $request);
}
