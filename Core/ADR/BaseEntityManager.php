<?php

namespace Core\ADR;

use Core\database\Manager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

abstract class BaseEntityManager {
    use Manager;
    
    protected EntityManager $em;
    protected array $repos = [];
    
    public function __construct(array $classes)
    {
        $this->em   = require CONFIG_PATH.'doctrine.php';
        foreach($classes as $name => $class) {
            $this->repos[$name] = $this->em->getRepository($class);
        }
    }
    
    
}
