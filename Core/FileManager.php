<?php

namespace Core;

class FileManager {
    
     private array $files;
     
     public function __construct()
     {
        $this->files = $_FILES;
     }
    
     public function getFiles(): array
     {
         return $this->files;
     }
     
     public function deleteFiles(string $type): void {
         unset($this->files);
         unset($_FILES);
     }
    
    public function get(string $key): ?array
    {
        return $this->files[$key] ?? null;
    }
    
}
