<?php

namespace Core\IO;

use Core\FileManager;
use function PHPUnit\Framework\throwException;

class Upload {
    
    public int $user_id;
    public array $allowed;
    public string $category;
    public string $filename;
    public string $filetype;
    public int $filesize;
    public string $tmp_name;
    public string $destination;
    
    public function __construct(int $user_id)
    {
        $fm = new FileManager();
        $avatar = $fm->get('avatar') ?? null;
        $cv = $fm->get('cv') ?? null;
        $this->user_id = $user_id;
    
        if(isset($avatar) && $avatar["error"] == 0) {
            $this->allowed = ["jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png"];
            if(in_array($avatar["type"], $this->allowed)){
                $ext = array_keys($this->allowed, $avatar['type']);
                $this->category = 'avatar';
                $this->filename = 'avatar.'.$ext[0];
                $this->filetype = $avatar["type"];
                $this->filesize = $avatar["size"];
                $this->tmp_name = $avatar["tmp_name"];
                $this->destination = IMAGE_PATH.'avatar'.DS;
            } else {
                die("Erreur : Veuillez sélectionner un format de fichier valide.");
            }
    
        } elseif (isset($cv) && $cv["error"] == 0) {
            $this->allowed = ["pdf" => "application/pdf"];
            if(in_array($cv["type"], $this->allowed)) {
                $ext = array_keys($this->allowed, $cv['type']);
                $this->category = 'cv';
                $this->filename = 'cv.'.$ext[0];
                $this->filetype = $cv["type"];
                $this->filesize = $cv["size"];
                $this->tmp_name = $cv["tmp_name"];
                $this->destination = DOCUMENT_PATH.'cv'.DS;
            } else {
                die("Erreur : Veuillez sélectionner un format de fichier valide.");
            }
        }
    
        return $this;
        
    }
    
    public function save(): Upload
    {
        $maxsize = 5 * 1024 * 1024;
        if($this->filesize > $maxsize) {
            throw new \Exception("Error: La taille du fichier est supérieure à la limite autorisée.");
        }
        move_uploaded_file($this->tmp_name, $this->destination . $this->user_id .'_'. $this->filename );
        
        return $this;
    }
    
    public function get(string $key)
    {
        return $this->$key;
    }
    
    
    
}
