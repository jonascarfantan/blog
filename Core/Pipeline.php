<?php

namespace Core;

use Core\ADR\ActionInterface;
use Nyholm\Psr7\Factory\Psr17Factory;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class Pipeline implements RequestHandlerInterface {
    
    private $container;
    private $middlewares = [];
    private $index = 0;
    private ResponseInterface $response;
    
    public function __construct(Psr17Factory $httpFactory, ContainerInterface $container) {
        $this->response  = $httpFactory->createResponse();
        $this->container = $container;
    }
    
    /**
     * Load new middleware
     *
     * @param callable|MiddlewareInterface $middleware
     */
    public function load($middleware): void {
        
        $this->middlewares[] = $middleware;
    }
    
    /**
     * @param $action
     */
    public function loadAction($action): void {
        $this->middlewares[] = $action;
    }
    
    private function getMiddleware() {
        if(isset($this->middlewares[$this->index])) {
            
            return $this->middlewares[$this->index];
        }
    }
    
    public function handle(ServerRequestInterface $request): ResponseInterface {
        $middleware = $this->getMiddleware();
        $this->index++;
        
        if($middleware instanceof MiddlewareInterface) {
            return $middleware->process($request, $this);
        }
        elseif($middleware instanceof ActionInterface) {
            $action = (new $middleware())
                ->setContainer($this->container)
                ->setResponder($this->container)
                ->setSession(new SessionManager($_SESSION));
            
            return $action($request);
        }
            return $this->response;
    }
}
