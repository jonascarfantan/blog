<?php

namespace Core;

use Core\routing\Route;
use Core\routing\Router;
use DI\Container;
use DI\ContainerBuilder;
use DI\DependencyException;
use DI\NotFoundException;
use Nyholm\Psr7\Factory\Psr17Factory;
use Nyholm\Psr7\Response;
use Nyholm\Psr7\ServerRequest;
use Nyholm\Psr7Server\ServerRequestCreator;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\Dotenv\Dotenv;

class App {
    
    public Container $container;
    public ServerRequest $request;
    public Psr17Factory $httpFactory;
    
    public function __construct()
    {
        $builder = new ContainerBuilder();
        $builder->useAutowiring(true);
        $builder->addDefinitions(CONFIG_PATH.'container.php');
        
        $this->container   = $builder->build();
        $this->request     = $this->container->get(ServerRequestCreator::class);
        $this->httpFactory = $this->container->get(Psr17Factory::class);
        $this->container->get(Dotenv::class);
    }
    public function run(): ResponseInterface
    {
        $router     = new Router($this->request, $this->container);
        $dispatcher = new Dispatcher($this->container, $router, $this->request);
        require '../routes/web.php';
        
        return $dispatcher->dispatch();
    }
}
