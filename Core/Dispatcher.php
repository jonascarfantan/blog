<?php

namespace Core;

use Core\Http\Response\Responder;
use Core\routing\Router;
use Nyholm\Psr7\Factory\Psr17Factory;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class Dispatcher {
    
    //___________________________________________
    //  *  Dispatcher retrieve the Request ________*/
    //  *  Parse the request uri and try to Match with Route or return 404_____*/
    //  *  Extract Route Middleware and push it in the Pipeline Handler queu */
    //  *  Extract Route Action / Parameters, prepare the final Handler of request life cycle
    // and append to the Pipeline Handler queu */
    // Then push the payload in the Pipeline queu and return a response */
    
    public ContainerInterface $container;
    public Router $router;
    public ServerRequestInterface $request;
    public Responder $responder;
    
    public function __construct(ContainerInterface $container, Router $router, ServerRequestInterface $request)
    {
        $this->container   = $container;
        $this->router      = $router;
        $this->request     = $request;
        $this->httpFactory = $container->get(Psr17Factory::class);
        $this->responder = new Responder($container);
    }
    
    /**
     * Call the router, match a route and get param or return a response,
     * load param in request, create pipeline, call middleware loader,
     * load action and launch request in the loaded stack
     *
     * @return ResponseInterface
     */
    public function dispatch()
    {
        $route_info = $this->router->run($this->httpFactory);
        if(!$route_info instanceof ResponseInterface) {
            $this->request = $this->request->withQueryParams($route_info);
            $pipeline      = new Pipeline($this->httpFactory, $this->container);
            
            require '../config/loadGlobalMiddleware.php';
            
            $this->loadSpecifiqueMiddleware($pipeline, $route_info['module']);
            $action = new $route_info['action']($this->container);
            try {
                $pipeline->loadAction($action);
                return $pipeline->handle($this->request);
            }catch(\Exception $e){
                return $this->responder->exception($e->getMessage(), $e->getCode());
            }
        }
        else {
            //Return a 404 response
            return $route_info;
        }
    }
    
    /**
     * @param Pipeline $pipeline
     * @param $module
     */
    private function loadSpecifiqueMiddleware(Pipeline $pipeline, $module): void
    {
        $requestMiddleware = $this->request->getQueryParams()['middleware'];
        if(isset($requestMiddleware)) {
            foreach($requestMiddleware as $middleware) {
                $middleware = "App\middleware\\".$middleware;
                $pipeline->load(new $middleware($this->container));
            }
        }
    }
}
