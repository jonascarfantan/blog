<?php

namespace Core\routing;

use Core\Http\Response\Responder;
use DI\Container;
use Nyholm\Psr7\Factory\Psr17Factory;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class Router {
    
    public Container $container;
    public ServerRequestInterface $request;
    public $path;
    public string $uri;
    public array $routes;
    public $matches;
    
    
    public function __construct(ServerRequestInterface $request, Container $container) {
        $this->container = $container;
        $this->request   = $request;
        
        //todo set a location redirection without ending slash in a middleware
        $this->uri = trim($this->request->getUri()->getPath(), '/');
    }
    
    /**
     * @param string $path
     * @param string $action
     */
    public function get(string $path, string $action, array $middleware = []) {
        $this->routes['GET'][] = new Route($path, $action, $middleware);
    }
    
    /**
     * @param string $path
     * @param string $action
     */
    public function post(string $path, string $action, array $middleware = []) {
        $this->routes['POST'][] = new Route($path, $action, $middleware);
    }
    
    /**
     * Router parse each route and determines which one corresponds to the request.
     * It extracts action, parameters and middleware to be used and
     * transfers them to the dispatcher.
     *
     * @param psr17factory $httpFactory
     *
     * @return array|ResponseInterface
     */
    public function run(Psr17Factory $httpFactory) {
        $method = $this->request->getMethod();
        foreach($this->routes[$method] as $route) {
            if(is_array($matched = $route->matches($this->uri))) {
                
                return $matched;
            }
        }
        $responder = $this->container->get(Responder::class);
        return $responder->exception('La page que vous souhaitez consulter n\'existe pas.', 404);
//        return $httpFactory->createResponse(419, 'Not found');
    }
    
}
