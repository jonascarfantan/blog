<?php

namespace Core\routing;

use Core\Http\Response\Responder;
use DI\Container;
use http\Env\Response;
use Nyholm\Psr7\Factory\Psr17Factory;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class Route {
    
    public string $path;
    public string $action;
    public array $middlewares;
    public array $matched;
    
    public function __construct(string $path, string $action, array $middlewares = [])
    {
        //todo make Location redirection instead of trim
        $this->path        = trim($path, '/');
        $this->action      = $action;
        $this->middlewares = $middlewares;
        
    }
    
    public function matches(string $uri)
    {
        $path      = preg_replace('#:([a-zA-Z0-9]+)#', '([^/]+)', $this->path);
        $regexPath = "#^$path$#";
        
        if(preg_match($regexPath, $uri, $matches)) {
            $this->matches = $matches;
            //todo gérer tous les types d'url notament ceux avec plusieur id ex: post/3/comment/8
            preg_match("#(?<=\\\App\\\).*(?=\\\action\\\)#", $this->action, $module_name);
            $roadmap = [
                'uri'        => $uri,
                'param'      => $matches[1] ?? null,
                'action'     => $this->action,
                'module'     => $module_name[0],
                'middleware' => $this->middlewares,
            ];
            
            return $roadmap;
        }
        else {
            return false;
        }
    }
    
}
