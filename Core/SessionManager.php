<?php

namespace Core;

/**
 * Class SessionManager
 *
 * @package Core
 */
class SessionManager {
    
     private array $session;
     
     public function __construct(array $session)
     {
        $this->session = &$session;
     }
     
    public static function startSession(int $lifetime, string $path, string $domain, bool $secure, bool $httpOnly)
    {
        session_set_cookie_params($lifetime, $path, $domain, $secure, $httpOnly);
        session_start();
    }
    
     public function getSession(): array
     {
         return $this->session;
     }
     
     public function connect(array $user_passport): self {
         foreach($user_passport as $key => $value){
             $this->$key = $value;
             $_SESSION[$key] = $value;
         }
         return $this;
     }
     
    public function disconnect(): self {
        $user_passport = [
            'pseudo','password','role','connected',
        ];
        foreach($user_passport as $key => $value){
            unset($_SESSION[$value]);
        }
        return $this;
    }
    
     public function newMessage(string $type, array $content): void {
         $_SESSION['message'][$type] = $content;
     }
     
     public function getMessages(string $type) {
         return $_SESSION['message'][$type] ?? null ;
     }
     
     public function deleteMessage(string $type): void {
         unset($_SESSION['message'][$type]);
     }
     
    public function set(string $key, string $value)
    {
      $this->session[$key] = $value;
      $_SESSION[$key] = $value;
      return $this;
    }
    
    public function get(string $key): ?string
    {
        return $this->session[$key] ?? null;
    }
    
}
