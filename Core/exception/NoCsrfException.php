<?php

namespace Core\exception;

class NoCsrfException extends \Exception {
    public function __construc(string $message, int $code){
        parent::__construct($message, $code);
    }
}
