# Projet n°5 du parcours Développeur dapplication web PHP / Smfony
# Blog personnel de Jonas Carfantan
Ce blog est écrit en PHP côté server et utilise les librairies Alpine js et Tailwind css côté navigateur.

# Installer l'environement de développement du projet
    - Vérifier que docker est corréctement installé sur votre machine.
    - Cloner le projet depuis gitlab à l'adresse : https://gitlab.com/jonascarfantan/blog
    - Depuis votre terminal à la racine du projet entrer les commandes suivantes:
        - docker-compose build
        - docker-compose up -d
        - docker-compose exec php bash
        - composer install
        - npm install
    - Ajouter l'addresse p5.local au localhost dans vos host

# Ajouter de la donnée de teste
    - Rendez-vous à l'adresse : p5.local:0880
    - Identifiants de connexion :
        id : root
        mdp : toor
    - Importer le fichier p5_dump_sql

# Connection
    - Vous pouvez vous inscrire avec un compte utilisateur.
    - Vous pouvez également utiliser le compte administrateur ci-dessous:
        - id : jury1
        - mdp : Azerty1234!
# Merci
Bonne navigation.
Jonas carfantan.
