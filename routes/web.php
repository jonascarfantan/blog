<?php
//HOME
$router->get('/', '\App\common\action\HomeAction',[]);
$router->get('/maintenance', '\App\common\action\MaintenancePageAction',[]);
$router->get('/contact', '\App\common\action\ContactPageAction',[]);
$router->post('/contact/send', '\App\common\action\SendContactEmailAction',[]);
//Landing Page Bo
$router->get('/admin/landing', '\App\common\action\LandingPageAction', ['LoggedMiddleware', 'AdminOnlyMiddleware']);
$router->post('/admin/landing/save', '\App\common\action\SaveLandingAction', ['LoggedMiddleware', 'AdminOnlyMiddleware']);


//SCRIPT
$router->get('/script/img/resize', '\App\common\action\ResizeImageAction',['LoggedMiddleware']);

//AUTH
$router->get('/connexion', '\App\auth\action\ConnexionPageAction', []);
$router->post('/register', '\App\auth\action\RegisterAction', []);
$router->post('/authentication', '\App\auth\action\AuthenticateAction', []);
$router->get('/logout', '\App\auth\action\LogoutAction', ['LoggedMiddleware']);


//User Fo
$router->get('/account/:id', '\App\auth\action\AccountPageAction', ['LoggedMiddleware']);
$router->get('/account/edit', '\App\auth\action\EditAccountPageAction', ['LoggedMiddleware']);
$router->post('/account/:id/update', '\App\auth\action\UpdateAccountAction', ['LoggedMiddleware']);
$router->post('/account/:id/update-password', '\App\auth\action\ChangePasswordAction', ['LoggedMiddleware']);
$router->post('/account/:id/upload', '\App\auth\action\UploadAction', ['LoggedMiddleware']);

//User BO
$router->get('/admin/users', '\App\auth\action\ManageUserAction', ['LoggedMiddleware', 'AdminOnlyMiddleware']);
$router->get('/admin/user/:id/remove', '\App\auth\action\RemoveUserAction', ['LoggedMiddleware', 'AdminOnlyMiddleware']);
$router->get('/admin/user/:id/switch-active', '\App\auth\action\SwitchUserStatusAction', ['LoggedMiddleware', 'AdminOnlyMiddleware']);


//BLOG POSTS FO
$router->get('/posts', '\App\post\action\ListPostAction', []);
$router->get('/post/:id', '\App\post\action\ReadPostAction', []);
//BLOG POSTS BO
$router->get('/admin/post/write', '\App\post\action\WritePostAction', ['LoggedMiddleware', 'AdminOnlyMiddleware']);
$router->get('/admin/posts', '\App\post\action\ManagePostAction', ['LoggedMiddleware', 'AdminOnlyMiddleware']);
$router->post('/admin/post/save', '\App\post\action\SavePostAction', ['LoggedMiddleware', 'AdminOnlyMiddleware']);
$router->get('/admin/post/:id/publish', '\App\post\action\PublishPostAction', ['LoggedMiddleware', 'AdminOnlyMiddleware']);
$router->get('/admin/post/:id/unpublish', '\App\post\action\UnpublishPostAction', ['LoggedMiddleware', 'AdminOnlyMiddleware']);
$router->get('/admin/post/:id/edit', '\App\post\action\EditPostAction', ['LoggedMiddleware', 'AdminOnlyMiddleware']);
$router->post('/admin/post/:id/update', '\App\post\action\UpdatePostAction', ['LoggedMiddleware', 'AdminOnlyMiddleware']);
$router->get('/admin/post/:id/delete', '\App\post\action\RemovePostAction', ['LoggedMiddleware', 'AdminOnlyMiddleware']);
//TAG
$router->post('/admin/tag/create', '\App\tag\action\NewTagAction', ['LoggedMiddleware', 'AdminOnlyMiddleware']);
$router->get('/admin/tag/:id/delete', '\App\tag\action\RemoveTagAction', ['LoggedMiddleware', 'AdminOnlyMiddleware']);
$router->get('/admin/tags', '\App\tag\action\ListTagAction', ['LoggedMiddleware', 'AdminOnlyMiddleware']);


//COMMENT FO
$router->post('/post/:id/comment', '\App\comment\action\CommentPostAction', ['LoggedMiddleware']);
$router->post('/post/:id/comment/:com_id', '\App\comment\action\RespondCommentAction', ['LoggedMiddleware']);
//COMMENT BO
$router->get('/admin/comments', '\App\comment\action\ManageCommentAction', ['LoggedMiddleware', 'AdminOnlyMiddleware']);
$router->post('/admin/comment/:id', '\App\comment\action\ModerateCommentAction', ['LoggedMiddleware', 'AdminOnlyMiddleware']);
$router->get('/admin/comment/:id/respond', '\App\comment\action\ResponseCommentAction', ['LoggedMiddleware', 'AdminOnlyMiddleware']);
$router->get('/admin/comment/:id/remove', '\App\comment\action\RemoveCommentAction', ['LoggedMiddleware', 'AdminOnlyMiddleware']);
$router->get('/admin/comment/:id/approve', '\App\comment\action\ApproveCommentAction', ['LoggedMiddleware', 'AdminOnlyMiddleware']);
$router->get('/admin/comment/:id/disapprove', '\App\comment\action\DisapproveCommentAction', ['LoggedMiddleware', 'AdminOnlyMiddleware']);



